﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using AF = Autofac.Features.Indexed;

namespace VidsSamp {
    public class Vids  { 
        
        private /* TRANSINFO: WithEvents */ SAPbouiCOM.Application SBO_Application; 
        private SAPbobsCOM.Company oCompany; 
        IBOMessageBox _messageBox;
        ITopMenu _topMenu;

        AF.IIndex<Tuple<string, string>, IView> _viewIndex;

        B1Tests.IMenuConfigurator _menuConfigurator;

        public Vids(SAPbouiCOM.Application app, SAPbobsCOM.Company company, IBOMessageBox messageBox, ITopMenu topMenu, B1Tests.IMenuConfigurator menuConfigurator, AF.IIndex<Tuple<string, string>, IView> viewIndex) { 
            if (app == null)
                throw new ArgumentNullException("app");
            if (company == null)
                throw new ArgumentNullException("company");
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            if (menuConfigurator == null)
                throw new ArgumentNullException("menuConfigurator");
            if (viewIndex == null)
                throw new ArgumentNullException("viewIndex");
            SBO_Application = app;
            oCompany = company;
            _messageBox = messageBox;
            _menuConfigurator = menuConfigurator;
            _topMenu = topMenu;
            _viewIndex = viewIndex;

            // events handled by SBO_Application_MenuEvent
			SBO_Application.MenuEvent += new SAPbouiCOM._IApplicationEvents_MenuEventEventHandler( SBO_Application_MenuEvent); 

            // events handled by SBO_Application_AppEvent
            SBO_Application.AppEvent += new SAPbouiCOM._IApplicationEvents_AppEventEventHandler( SBO_Application_AppEvent);
        
            // events handled by SBO_Application_ItemEvent
            SBO_Application.ItemEvent += new SAPbouiCOM._IApplicationEvents_ItemEventEventHandler( SBO_Application_ItemEvent ); 
        }

        public void Run()
        {
            AddMenuItems(_topMenu); 
            _messageBox.Notify( "DI Connected To: " + oCompany.CompanyName, 1, "Ok", "", "" ); 
        }

        private void AddMenuItems(ITopMenu topMenu) { 
            // //******************************************************************
            // // Let's add a pop-up menu item and 3 sub menu items
            // //******************************************************************

            try {
                var oMenuItem = topMenu.FindMenuItem("43520"); // moudles'
                var oMenus = topMenu.GetSubMenus("43520"); 

                //  If the manu already exists this code will fail
                _menuConfigurator.WithNewPopup("SM_VID")
                    .String("Video Store")
                    .Image("VID.bmp")
                    .Position(topMenu.GetSubMenusCount("43520") + 1)
                    .AddAsChildOf(oMenus);
                
                //  Get the menu collection of the newly added pop-up item
                var smVidMenus = topMenu.GetSubMenus("SM_VID"); 
                
                //  Add 4 Sub Menu Items
                _menuConfigurator.WithNewString("SM_VID_F1")
                    .String("Movies On Shelf")
                    .Image("v1.bmp")
                    .AddAsChildOf(smVidMenus);

                _menuConfigurator.WithNewString("SM_VID_F2")
                    .String("Movies Location")
                    .Image("v2.bmp")
                    .AddAsChildOf(smVidMenus);
                
                _menuConfigurator.WithNewString("SM_VID_F3")
                    .String("Add Movie")
                    .Image("v3.bmp")
                    .AddAsChildOf(smVidMenus);

                _menuConfigurator.WithNewString("SM_VID_F4")
                    .String("Rent / Return Movie")
                    .Image("v4.bmp")
                    .AddAsChildOf(smVidMenus);                
            } 
            catch ( Exception er ) { //  Error Handling
                _messageBox.Notify(er.Message, 1, "Ok", "", "");
            } 
        }

        void ActivateView(string formUID)
        {
            var view = _viewIndex[Tuple.Create(formUID, String.Empty)];
            if (view == null)
                throw new InvalidOperationException(String.Format("{0} view not registered", formUID));
            view.Activate();
        }

		private void SBO_Application_MenuEvent( ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent )
		{
			BubbleEvent = true;
            if ( pVal.BeforeAction == false ) { 
                switch ( pVal.MenuUID ) {
                    case "SM_VID_F1":
                        ActivateView("vids_1");
                        break;
                    case "SM_VID_F2":
                        ActivateView("vids_2");
                        break;
                    case "SM_VID_F3":
                        ActivateView("vids_3");
                        break;
                    case "SM_VID_F4":
                        ActivateView("vids_4");
                        break;
                }
            }
        }

        private void SBO_Application_AppEvent( SAPbouiCOM.BoAppEventTypes EventType ) { 
            //  Terminate our addon when the application shuts down
            if ( EventType == SAPbouiCOM.BoAppEventTypes.aet_ShutDown ) { 
                _messageBox.Notify("Vids Addon will now terminate", 1, "Ok", "", "");
                System.Windows.Forms.Application.Exit(); 
            }
        }
        
        private void SBO_Application_ItemEvent( string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent ) { 
            BubbleEvent = true;
            if ( pVal.BeforeAction == false ) {
                // NOTE: instead of this check, an event filter should
                // be set
                IView view = null;
                if (_viewIndex.TryGetValue(Tuple.Create(FormUID, pVal.FormTypeEx), out view))
                {
                    view.HandleItemEvent(ref pVal);
                }
            }                
        } 
    } 
} 
