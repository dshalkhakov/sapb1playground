﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IUserTableService
    {
        bool UserTableExists(string id);
        SAPbobsCOM.UserTable GetUserTable(string id);
        void SetField<T>(SAPbobsCOM.UserTable userTable, string key, T value);
    }

    public class UserTableService : IUserTableService
    {
        SAPbobsCOM.Company _company;

        public UserTableService(SAPbobsCOM.Company company)
        {
            if (company == null)
                throw new ArgumentNullException();
            _company = company;
        }

        public bool UserTableExists(string id)
        {
            bool ret;
            try
            {
                _company.UserTables.Item(id);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }

        public SAPbobsCOM.UserTable GetUserTable(string id)
        {
            return _company.UserTables.Item(id);
        }

        public void SetField<T>(SAPbobsCOM.UserTable userTable, string key, T value)
        {
            userTable.UserFields.Fields.Item(key).Value = value;
        }
    }
}
