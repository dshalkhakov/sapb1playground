﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace B1Tests
{
    public class App
    {
        static SAPbouiCOM.SboGuiApi RegisterGuiApi(IComponentContext ctx)
        {
            var c = new SAPbouiCOM.SboGuiApi();
#if DEBUG
            var connStr = "0030002C0030002C00530041005000420044005F00440061007400650076002C0050004C006F006D0056004900490056";
#else
            var connStr = System.Environment.CommandLine[1].ToString();
#endif
            c.Connect(connStr);
            return c;
        }

        static SAPbouiCOM.Application GetApplication(IComponentContext ctx)
        {
#if DEBUG
            var appID = -1;
#else
            var appID = 1;
#endif
            var app = ctx.Resolve<SAPbouiCOM.SboGuiApi>().GetApplication(appID);
            
            if (app.ClientType != SAPbouiCOM.BoClientType.ct_Browser)
            {
                ProcessUtility.KillStrayProcesses();
            }
            return app;
        }

        static SAPbobsCOM.Company ConnectToCompany(IComponentContext ctx)
        {
            var company = new SAPbobsCOM.Company(); 
            var sCookie = company.GetContextCookie(); 

            var sConnectionContext = ctx.Resolve<SAPbouiCOM.Application>().Company.GetConnectionContext(sCookie); 

            if ( company.Connected == true ) { 
                company.Disconnect(); 
            } 

            // // Set the connection context information to the DI API.
            var ret = company.SetSboLoginContext(sConnectionContext); 
            if (!(ret == 0))
                throw new Exception("DI connection failed");

            var ret2 = company.Connect();
            if (!(ret2 == 0))
                throw new Exception("Couldn't connect to company");

            return company;
        }

        public static IContainer GetApp()
        {
            var container = new ContainerBuilder();

            container.RegisterType<VidsSamp.UserTableService>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            container.RegisterType<VidsSamp.FileLoader>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            container.RegisterType<VidsSamp.StartupPathProvider>()
                .AsImplementedInterfaces()
                .InstancePerDependency();
            
            container.Register<SAPbouiCOM.SboGuiApi>(RegisterGuiApi)
                .AsSelf()
                .InstancePerLifetimeScope();
            
            container.Register<SAPbouiCOM.Application>(GetApplication)
                .AsSelf()
                .InstancePerLifetimeScope();

            container.RegisterType<B1Tests.MenuItemConfigurator>()
                .AsImplementedInterfaces()
                .InstancePerDependency();
            container.RegisterType<B1Tests.MenuConfigurator>()
                .AsImplementedInterfaces()
                .InstancePerDependency();
            container.RegisterType<VidsSamp.FormService>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            // DI connection context, aka Company
            container.Register<SAPbobsCOM.Company>(ConnectToCompany).AsSelf()
                .InstancePerLifetimeScope();

            container.RegisterType<VidsSamp.UserDefinedTables>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            container.RegisterType<VidsSamp.BOMessageBox>()
                .As<VidsSamp.IBOMessageBox>()
                .InstancePerDependency();

            container.RegisterType<VidsSamp.BOTopMenu>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            container.RegisterFormViews(System.Reflection.Assembly.GetExecutingAssembly());
            container.RegisterPresenters(System.Reflection.Assembly.GetExecutingAssembly());

            container.RegisterType<VidsSamp.Vids>()
                .AsSelf()
                .InstancePerLifetimeScope();

            container.RegisterType<VidsSamp.ActiveFormService>()
                .AsImplementedInterfaces()
                .InstancePerDependency();

            return container.Build();
        }
    }
}
