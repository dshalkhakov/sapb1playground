﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IRentMovieService
    {
        bool IsMovieRented(string code);
        void RentMovie(string MovieStr, string CardStr);
        void ReturnMovie(string MovieStr);
    }

    public class RentMovieService : IRentMovieService
    {
        SAPbobsCOM.Company oCompany;

        public RentMovieService(SAPbobsCOM.Company company)
        {
            if (company == null)
                throw new ArgumentException("company");
            oCompany = company;
        }

        public bool IsMovieRented(string MovieStr)
        {
            // corner case: movie does not exist
            var RecSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            var QryStr = "select U_RENTED from [@VIDS] where Code = '" + MovieStr + "'";
            RecSet.DoQuery(QryStr);
            var QryAns = System.Convert.ToString(RecSet.Fields.Item(0).Value);
            return QryAns.Equals("Y");
        }

        public void RentMovie(string MovieStr, string CardStr)
        {
            var RecSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            var QryStr = "update [@VIDS] set U_RENTED = 'Y', U_CARDCODE = '" + CardStr + "' where Code = '" + MovieStr + "'";
            RecSet.DoQuery(QryStr);
        }

        public void ReturnMovie(string MovieStr)
        {
            var RecSet = ((SAPbobsCOM.Recordset)(oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)));
            var QryStr = "update [@VIDS] set U_RENTED = 'N', U_CARDCODE = '' where Code = '" + MovieStr + "'";
            RecSet.DoQuery(QryStr);
        }
    }
}
