﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IMoviesToShelfForm : IView
    {
        bool ShowRented();
        int? ShowShelfNum();
        void FilterMovies(string movieCode, string combCode);
    }

    [View("vids_1", "")]
    public class MoviesToShelfForm : IMoviesToShelfForm
    {
        IBOMessageBox _messageBox;
        IFormService _formService;
        IFileLoader _fileLoader;
        IMoviesToShelfPresenter _presenter;
        MoviesToShelfPresenter.Factory _presenterFactory;
        ITopMenu _topMenu;
        IActiveFormService _activeForm;

        public MoviesToShelfForm(IBOMessageBox messageBox, IFormService formService, IFileLoader fileLoader, MoviesToShelfPresenter.Factory presenterFactory, ITopMenu topMenu, IActiveFormService activeForm)
        {
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            if (formService == null)
                throw new ArgumentNullException("formService");
            if (fileLoader == null)
                throw new ArgumentNullException("fileLoader");
            if (presenterFactory == null)
                throw new ArgumentNullException("presenterFactory");
            if (topMenu == null)
                throw new ArgumentNullException("topMenu");
            if (activeForm == null)
                throw new ArgumentNullException("activeForm")
;            _messageBox = messageBox;
            _formService = formService;
            _fileLoader = fileLoader;
            _presenterFactory = presenterFactory;
            _topMenu = topMenu;
            _activeForm = activeForm;
        }

        public void HandleItemEvent(ref SAPbouiCOM.ItemEvent pVal)
        {
            if ( ( ( pVal.ItemUID == "cmb_shelfs" ) | ( pVal.ItemUID == "cmb_rented" ) ) & pVal.EventType == SAPbouiCOM.BoEventTypes.et_COMBO_SELECT ) { 
                _presenter.ShowMoviesByShelf(); 
            }
            if ( ( ( pVal.ItemUID == "btnAdd" ) & ( pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED ) ) ) { 
                _topMenu.ActivateMenuItem( "SM_VID_F3" ); 
            } 
        }

        private string LoadFromXML( string FileName ) { 
            // FIXME error checks
            var oXmlDoc = _fileLoader.LoadXMLFromStartupPath(FileName);
            
            // // load the form to the SBO application in one batch
            return ( oXmlDoc.InnerXml ); 
        } 

        // checks if "vids_1" form exists
        // if it does, brings it to front
        // if not, creates it
        public void Activate()
        {
            // is this style more LoD compliant or what?
            _formService.WithForm("vids_1",
                form => {
                    form.Top = 150; 
                    form.Left = 330; 
                    form.Visible = true;
                },
                fcp => {
                    fcp.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed; 
                    fcp.FormType = "Vids"; 
                    fcp.XmlData = LoadFromXML( "MoviesToShelf.xml" ); 
                },
                form => {
                    // This is a bug, The XML property of AffectsFormMode is ignored.
                    // FIXME: violates LoD, refactor
                    var i = form.Items.Item( "cmb_shelfs" ); 
                    var m = ( ( SAPbouiCOM.ComboBox )( i.Specific ) ); 
                    i.AffectsFormMode = false; 

                    GetDataFromDataSource( form ); 

                    _presenter = _presenterFactory(this);
                });
        }

        public void BindDataToForm( SAPbouiCOM.Columns oColumns ) { 
            SAPbouiCOM.Column oColumn = null; 
            
            // // getting the matrix column by the UID
            
            oColumn = oColumns.Item( "ItemCode" ); 
            oColumn.DataBind.SetBound( true, "@VIDS", "Code" ); 
            
            oColumn = oColumns.Item( "MovieName" ); 
            oColumn.DataBind.SetBound( true, "@VIDS", "Name" ); 
            
            oColumn = oColumns.Item( "CardCode" ); 
            oColumn.DataBind.SetBound( true, "@VIDS", "U_CARDCODE" ); 
            
            oColumn = oColumns.Item( "Shelf" ); 
            oColumn.DataBind.SetBound( true, "@VIDS", "U_SHELF" ); 
            
            oColumn = oColumns.Item( "Space" ); 
            oColumn.DataBind.SetBound( true, "@VIDS", "U_SPACE" ); 
            
            oColumn = oColumns.Item( "Rented" ); 
            oColumn.DataBind.SetBound( true, "@VIDS", "U_RENTED" ); 
            
        } 

        public bool ShowRented()
        {
        // SBO_Application.ActiveForm_CmbRented :: unit -> SAPbouiCOM.ComboBox
            return false;
        }

        public int? ShowShelfNum()
        {
            return 0;
        }

        public void FilterMovies(string a, string b)
        {

        }

        public void GetDataFromDataSource( SAPbouiCOM.Form oForm ) { 
            
            SAPbouiCOM.Matrix oMatrix = null; 
            SAPbouiCOM.Item oItem = null; 
            
            oItem = oForm.Items.Item( "mat" ); 
            oMatrix = ( ( SAPbouiCOM.Matrix )( oItem.Specific ) ); 
            
            // Set The Form Binding
            var DBDSVids = oForm.DataSources.DBDataSources.Add( "@VIDS" ); 
            BindDataToForm( oMatrix.Columns ); 
            
            // // Ready Matrix to populate data
            oMatrix.Clear(); 
            oMatrix.AutoResizeColumns(); 
            
            // // Querying the DB Data source
            DBDSVids.Query( null ); 
            
            oMatrix.LoadFromDataSource(); 
        } 
    }
}
