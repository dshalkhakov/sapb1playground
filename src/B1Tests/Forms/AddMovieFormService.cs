﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IAddMovieFormService
    {
        bool AddMovieToDatabase(string Code, string Name, int Shelf, int Space);
    }

    public class AddMovieFormService : IAddMovieFormService
    {
        IUserTableService _userTable;
        IBOMessageBox _messageBox;

        public AddMovieFormService(IUserTableService userTable, IBOMessageBox messageBox)
        {
            if (userTable == null)
                throw new ArgumentNullException("userTable");
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            _userTable = userTable;
            _messageBox = messageBox;
        }

        public bool AddMovieToDatabase( string Code, string Name, int Shelf, int Space ) {             
            var oUsrTbl = _userTable.GetUserTable("VIDS");
            
            oUsrTbl.Code = System.Convert.ToString( int.Parse( Code ) ); 
            oUsrTbl.Name = Name; 
            _userTable.SetField(oUsrTbl, "U_SHELF", Shelf);
            _userTable.SetField(oUsrTbl, "U_SPACE", Space);
            var Res = oUsrTbl.Add(); 
            
            if (Res != 0)
                _messageBox.Notify( "Error, failed to add movie", 1, "Ok", "", "" ); 
            return Res == 0;
        }
    }
}
