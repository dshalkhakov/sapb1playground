﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IAddMovieFormView : IView
    {
        string GetSearchMovieCode();
        string GetSearchMovieName();
        int GetSearchShelf();
        int GetSearchSpace();
        bool MovieExists(string strFind);
    }

    [View("vids_3", "")]
    // Passive view.
    public class AddMovieFormView : IAddMovieFormView
    {
        IFormService _formService;
        AddMovieFormPresenter.Factory _addMoviePresenterFactory;
        IAddMovieFormPresenter _addMoviePresenter;
        IActiveFormService _activeForm;

        public AddMovieFormView(IFormService formService, AddMovieFormPresenter.Factory addMoviePresenterFactory, IActiveFormService activeForm)
        {
            if (formService == null)
                throw new ArgumentNullException("formService");
            if (addMoviePresenterFactory == null)
                throw new ArgumentNullException("addMoviePresenterFactory");
            if (activeForm == null)
                throw new ArgumentNullException("activeForm");
            _formService = formService;
            _addMoviePresenterFactory = addMoviePresenterFactory;
            _activeForm = activeForm;
        }

        public void HandleItemEvent(ref SAPbouiCOM.ItemEvent pVal)
        {
            if ( ( ( pVal.ItemUID == "btnAdd" ) & ( pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED ) ) ) { 
                _addMoviePresenter.AddMovie(); 
            } 
        }

        public void Activate() { 
            _formService.WithForm("vids_3",
                oForm => {
                    oForm.Visible = true;             
                },
                fcp => {
                    fcp.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed; 
                    fcp.FormType = "Vids3"; 
                },
                oForm => {
                    oForm.AutoManaged = false; 
                
                    DrawAddForm( oForm );

                    _addMoviePresenter = _addMoviePresenterFactory(this);
                }
            );
        } 

        // internal representation of the form
        // DBDSVids @VIDS :: DBDataSource
        // lblCode :: StaticText -- String
        // lblName :: StaticText -- String
        // lblShelf :: StaticText -- String
        // lblSpace :: StaticText -- String
        // txtCode :: EditText -- String
        // txtName :: EditText -- String
        // txtShelf :: EditText -- String
        // txtSpace :: EditText -- String
        // btnAdd :: Button

        // MVP inputs set: { txtCode, txtName, txtShelf }
        // MVP outputs set: { }
        // MVP actions set: { add }

        // view: getTxtCode : () -> string
        //       getTxtName : () -> string
        //       getTxtShelf : () -> int
        // presenter: addMovie()
        // service: movieExists(code, name, shelf) : bool

        private void DrawAddForm( SAPbouiCOM.Form oForm ) { 
            SAPbouiCOM.Item oItem = null; 
            SAPbouiCOM.StaticText oLabel = null; 
            SAPbouiCOM.Button oButton = null; 
            
            oForm.Top = 150; 
            oForm.Left = 330; 
            oForm.ClientWidth = 200; 
            oForm.ClientHeight = 170; 
            oForm.Title = "Add Movie"; 
            
            // Adding a datasource to the form
            var DBDSVids = oForm.DataSources.DBDataSources.Add( "@VIDS" ); 
            
            //  Adding 4 Static Texts
            //  Movie Code
            oItem = oForm.Items.Add( "lblCode", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 10; 
            oItem.AffectsFormMode = false; 
            oLabel = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oLabel.Caption = "Movie Code"; 
            
            //  Name
            oItem = oForm.Items.Add( "lblName", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 40; 
            oItem.AffectsFormMode = false; 
            oLabel = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oLabel.Caption = "Movie Name"; 
            
            //  Shelf
            oItem = oForm.Items.Add( "lblShelf", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 70; 
            oItem.AffectsFormMode = false; 
            oLabel = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oLabel.Caption = "Shelf"; 
            
            //  Space
            oItem = oForm.Items.Add( "lblSpace", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 100; 
            oItem.AffectsFormMode = false; 
            oLabel = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oLabel.Caption = "Space"; 
            
            //  Add 3 Edit Texts
            //  Movie Code
            oItem = oForm.Items.Add( "txtCode", SAPbouiCOM.BoFormItemTypes.it_EDIT ); 
            oItem.Left = 100; 
            oItem.Top = 10; 
            oItem.AffectsFormMode = false; 
            
            //  Name
            oItem = oForm.Items.Add( "txtName", SAPbouiCOM.BoFormItemTypes.it_EDIT ); 
            oItem.Left = 100; 
            oItem.Top = 40; 
            oItem.AffectsFormMode = false; 
            
            //  Shelf
            oItem = oForm.Items.Add( "txtShelf", SAPbouiCOM.BoFormItemTypes.it_EDIT ); 
            oItem.Left = 100; 
            oItem.Top = 70; 
            oItem.AffectsFormMode = false; 
            
            //  Space
            oItem = oForm.Items.Add( "txtSpace", SAPbouiCOM.BoFormItemTypes.it_EDIT ); 
            oItem.Left = 100; 
            oItem.Top = 100; 
            oItem.AffectsFormMode = false; 
            
            //  Adding "Add Movie" button
            oItem = oForm.Items.Add( "btnAdd", SAPbouiCOM.BoFormItemTypes.it_BUTTON ); 
            oItem.Left = 10; 
            oItem.Top = 130; 
            oItem.AffectsFormMode = false; 
            oButton = ( ( SAPbouiCOM.Button )( oItem.Specific ) ); 
            oButton.Caption = "Add Movie"; 
        }

        #region Query methods. MVP inputs

        public string GetSearchMovieCode()
        {
            return _activeForm.GetEditTextString("txtCode");
        }

        public string GetSearchMovieName()
        {
            return _activeForm.GetEditTextString("txtName");
        }

        public int GetSearchShelf()
        {
            return int.Parse(_activeForm.GetEditTextString("txtShelf"));
        }

        public int GetSearchSpace()
        {
            return int.Parse(_activeForm.GetEditTextString("txtSpace"));
        }

        // if strFind is input, should check if movie of this code already exists
        // if strFind is null or empty, raises ArgumentNullException
        public bool MovieExists(string strFind)
        {
            if (String.IsNullOrEmpty(strFind))
                throw new ArgumentNullException("strFind");

            var DBDSVids = _activeForm.GetDBDataSource("@VIDS");
            DBDSVids.Query(null);

            int i = 0;
            var isFound = false;

            for (i = 0; i < DBDSVids.Size; i++)
            {
                if ((DBDSVids.GetValue("Code", i).Trim(char.Parse(" ")) == strFind))
                {
                    isFound = true;
                    break; /* TRANSWARNING: check that break is in correct scope */
                }
            }
            return isFound;
        }

        #endregion
    }
}
