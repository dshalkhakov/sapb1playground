﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IRentMovieForm : IView
    {
        string GetSelectedMovie();
        string GetSelectedCard();
        void ResetSelection();
        void ResetSearch();
        void Notify(string s);
    }

    [View("vids_4", "")]
    public class RentMovieForm : IRentMovieForm
    {
        IBOMessageBox _messageBox;
        SAPbobsCOM.Company oCompany;
        IFormService _formService;
        IActiveFormService _activeForm;
        IRentMovieFormPresenter _presenter;
        RentMovieFormPresenter.Factory _presenterFactory;

        public RentMovieForm(IBOMessageBox messageBox, SAPbobsCOM.Company company, IFormService formService, IActiveFormService activeForm, RentMovieFormPresenter.Factory presenterFactory)
        {
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            if (company == null)
                throw new ArgumentNullException("company");
            if (formService == null)
                throw new ArgumentNullException("formService");
            if (activeForm == null)
                throw new ArgumentNullException("activeForm");
            if (presenterFactory == null)
                throw new ArgumentNullException("presenterFactory");
            _messageBox = messageBox;
            oCompany = company;
            _formService = formService;
            _activeForm = activeForm;
            _presenterFactory = presenterFactory;
        }

        public void HandleItemEvent(ref SAPbouiCOM.ItemEvent pVal)
        {
            if ( ( ( pVal.ItemUID == "btnRent" ) & ( pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED ) ) ) {
                _presenter.RentMovie();
            } 
            if ( ( ( pVal.ItemUID == "btnRet" ) & ( pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED ) ) ) {
                _presenter.ReturnMovie();
            } 
        }

        public void Activate() { 
            _formService.WithForm("vids_4",
                oForm => {
                    oForm.Visible = true; 
                },
                fcp => {
                    fcp.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed; 
                    fcp.FormType = "Vids4"; 
                },
                oForm => {
                    oForm.AutoManaged = false; 
                
                    DrawRentForm( oForm );

                    _presenter = _presenterFactory(this);
                });
        }

        public void Notify(string s)
        {
            _messageBox.Notify(s, 1, "Ok", "", ""); 
        }

        public string GetSelectedMovie()
        {
            return _activeForm.GetComboBoxSelectedValue("CombCode");
        }

        public string GetSelectedCard()
        {
            return _activeForm.GetComboBoxSelectedDescription("CombCard");
        }

        public void ResetSelection()
        {
            var oMovieCombo = _activeForm.GetComboBox("CombCode");
            var oCardCombo = _activeForm.GetComboBox("CombCard");
            oMovieCombo.Select("0", (SAPbouiCOM.BoSearchKey)(0));
            oCardCombo.Select("0", (SAPbouiCOM.BoSearchKey)(0)); 
        }

        public virtual void ResetSearch()
        {
            var oMovieCombo = _activeForm.GetComboBox("CombCode");
            oMovieCombo.Select("0", (SAPbouiCOM.BoSearchKey)(0));
        }

        private void DrawRentForm( SAPbouiCOM.Form oForm ) { 
            SAPbouiCOM.Item oItem = null; 
            SAPbouiCOM.StaticText oLabel = null; 
            SAPbouiCOM.EditText oEdit = null; 
            SAPbouiCOM.Button oButton = null; 
            SAPbouiCOM.LinkedButton oLinked = null; 
            SAPbouiCOM.ComboBox oCombCard = null; 
            SAPbouiCOM.ComboBox oCombMovie = null; 
            
            oForm.Top = 150; 
            oForm.Left = 330; 
            oForm.ClientWidth = 200; 
            oForm.ClientHeight = 130; 
            oForm.Title = "Rent/Return Movie"; 
            
            // Adding a datasource to the form
            var DBDSVids = oForm.DataSources.DBDataSources.Add( "@VIDS" ); 
            
            //  Adding 3 Static Texts
            //  Movie Code
            oItem = oForm.Items.Add( "lblCode", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 10; 
            oItem.AffectsFormMode = false; 
            oLabel = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oLabel.Caption = "Movie"; 
            
            //  CardCode
            oItem = oForm.Items.Add( "lblShelf", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 40; 
            oItem.AffectsFormMode = false; 
            oLabel = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oLabel.Caption = "Client Code"; 
            
            //  Add Movie Code Combo Box
            oItem = oForm.Items.Add( "CombCode", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX ); 
            oItem.Left = 100; 
            oItem.Top = 10; 
            oItem.AffectsFormMode = false; 
            oItem.DisplayDesc = true; 
            oCombCard = ( ( SAPbouiCOM.ComboBox )( oItem.Specific ) ); 
            
            //  Add Movie Code Combo Box
            oItem = oForm.Items.Add( "CombCard", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX ); 
            oItem.Left = 100; 
            oItem.Top = 40; 
            oItem.AffectsFormMode = false; 
            oItem.DisplayDesc = true; 
            oCombMovie = ( ( SAPbouiCOM.ComboBox )( oItem.Specific ) ); 
            
            //  Adding "Rent Movie" button
            oItem = oForm.Items.Add( "btnRent", SAPbouiCOM.BoFormItemTypes.it_BUTTON ); 
            oItem.Left = 10; 
            oItem.Top = 90; 
            oItem.AffectsFormMode = false; 
            oButton = ( ( SAPbouiCOM.Button )( oItem.Specific ) ); 
            oButton.Caption = "Rent Movie"; 
            
            //  Adding "Return Movie" button
            oItem = oForm.Items.Add( "btnRet", SAPbouiCOM.BoFormItemTypes.it_BUTTON ); 
            oItem.Left = 90; 
            oItem.Top = 90; 
            oItem.AffectsFormMode = false; 
            oButton = ( ( SAPbouiCOM.Button )( oItem.Specific ) ); 
            oButton.Caption = "Return Movie"; 
            
            LoadMoviesCodeComboVals( oCombCard ); 
            LoadCardCodeComboVals( oCombMovie ); 
            
        } 

        private void LoadCardCodeComboVals( SAPbouiCOM.ComboBox oCombo ) { 
            // FIXME: explicit 'new'
            var RecSet = ( ( SAPbobsCOM.Recordset )( oCompany.GetBusinessObject( SAPbobsCOM.BoObjectTypes.BoRecordset ) ) ); 
            RecSet.DoQuery( "Select CardCode from OCRD where CardType = 'C'" ); 
            var RecCount = RecSet.RecordCount; 
            RecSet.MoveFirst(); 
            
            // Add the default value
            oCombo.ValidValues.Add( "0", "Select Client" ); 
            
            for ( int i = 0; i < RecCount; i++ ) {
                // NOTE: types casts might be invalid
                oCombo.ValidValues.Add( (i + 1).ToString(), (string)RecSet.Fields.Item( 0 ).Value ); 
                RecSet.MoveNext(); 
            }
            
            oCombo.Select( "0", SAPbouiCOM.BoSearchKey.psk_ByValue ); 
        } 

        private void LoadMoviesCodeComboVals( SAPbouiCOM.ComboBox oCombo ) {             
            // FIXME: explicit 'new'
            var RecSet = ( ( SAPbobsCOM.Recordset )( oCompany.GetBusinessObject( SAPbobsCOM.BoObjectTypes.BoRecordset ) ) ); 
            RecSet.DoQuery( "Select Code, Name from [@VIDS]" ); 
            var RecCount = RecSet.RecordCount; 
            RecSet.MoveFirst(); 
            
            // Add the default value
            oCombo.ValidValues.Add( "0", "Select Movie" ); 
            
            for ( int i = 0; i < RecCount; i++ ) { 
                oCombo.ValidValues.Add( System.Convert.ToString( RecSet.Fields.Item( 0 ).Value ), System.Convert.ToString( RecSet.Fields.Item( 1 ).Value ) ); 
                RecSet.MoveNext(); 
            } 
            
            oCombo.Select( "0", SAPbouiCOM.BoSearchKey.psk_ByValue ); 
        } 

    }
}
