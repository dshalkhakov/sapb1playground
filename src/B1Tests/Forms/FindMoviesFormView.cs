﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IFindMoviesForm : IView
    {
        string GetMovieCode();

        void SetShelf(int shelf);
        void SetSpace(int space);
        void AddBPLinkValidValue(string code);
    }

    [View("vids_2", "")]
    public class FindMoviesForm : IFindMoviesForm
    {
        IBOMessageBox _messageBox;
        IStartupPathProvider _startupPathProvider;
        IFormService _formService;
        IActiveFormService _activeForm;
        FindMoviesFormPresenter.Factory _presenterFactory;
        IFindMoviesFormPresenter _presenter;

        public FindMoviesForm(IBOMessageBox messageBox, IStartupPathProvider startupPathProvider, IFormService formService, FindMoviesFormPresenter.Factory presenterFactory, IActiveFormService activeForm)
        {
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            if (startupPathProvider == null)
                throw new ArgumentNullException("startupPathProvider");
            if (formService == null)
                throw new ArgumentNullException("formService");
            if (presenterFactory == null)
                throw new ArgumentNullException("presenterFactory");
            if (activeForm == null)
                throw new ArgumentNullException("activeForm");
            _messageBox = messageBox;
            _startupPathProvider = startupPathProvider;
            _formService = formService;
            _presenterFactory = presenterFactory;
            _activeForm = activeForm;
        }

        public void HandleItemEvent(ref SAPbouiCOM.ItemEvent pVal)
        {
            if ( ( ( pVal.ItemUID == "btnFind" ) & ( pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED ) ) ) { 
                _presenter.FindMovie(); 
            } 
        }

        public void Activate()
        {
            _formService.WithForm("vids_2",
                oForm => {
                    oForm.Visible = true;
                },
                fcp => {
                    fcp.BorderStyle = SAPbouiCOM.BoFormBorderStyle.fbs_Fixed; 
                    fcp.FormType = "Vids2"; 
                },
                oForm => {
                    oForm.AutoManaged = false; 
                
                    DrawFindForm( oForm );

                    _presenter = _presenterFactory(this);
                });
        } 

        public void AddBPLinkValidValue(string BP)
        {
            var oCombo = _activeForm.GetComboBox("BPLink");
            oCombo.ValidValues.Add(BP, "");
            oCombo.Select(BP, (SAPbouiCOM.BoSearchKey)(0)); 
        }

        public string GetMovieCode()
        {
            return _activeForm.GetEditTextString("txtCode");
        }

        public void SetShelf(int shelf)
        {
            _activeForm.SetEditTextString("txtShelf", shelf.ToString());
        }

        public void SetSpace(int space)
        {
            _activeForm.SetEditTextString("txtspace", space.ToString());
        }

                    // Here we will create a UI form by code and not by Loading XML
        private void DrawFindForm( SAPbouiCOM.Form oForm ) { 
            SAPbouiCOM.Item oItem = null; 
            SAPbouiCOM.StaticText oStat = null; 
            SAPbouiCOM.EditText oEdit = null; 
            SAPbouiCOM.Button oButton = null; 
            SAPbouiCOM.LinkedButton oLinked = null; 
            
            oForm.Top = 150; 
            oForm.Left = 330; 
            oForm.ClientWidth = 200; 
            oForm.ClientHeight = 170; 
            oForm.Title = "Find Movie Location"; 
            
            // Adding a datasource to the form
            var DBDSVids = oForm.DataSources.DBDataSources.Add( "@VIDS" ); 
            
            //  Adding 4 Static Texts
            //  Movie Code
            oItem = oForm.Items.Add( "lblCode", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 10; 
            oItem.AffectsFormMode = false; 
            oStat = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oStat.Caption = "Movie Code"; 
            
            //  Shelf
            oItem = oForm.Items.Add( "lblShelf", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 70; 
            oItem.AffectsFormMode = false; 
            oStat = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oStat.Caption = "Shelf"; 
            
            //  Space
            oItem = oForm.Items.Add( "lblSpace", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 90; 
            oItem.AffectsFormMode = false; 
            oStat = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oStat.Caption = "Space"; 
            
            //  Space
            oItem = oForm.Items.Add( "lblBP", SAPbouiCOM.BoFormItemTypes.it_STATIC ); 
            oItem.Left = 10; 
            oItem.Top = 110; 
            oItem.AffectsFormMode = false; 
            oStat = ( ( SAPbouiCOM.StaticText )( oItem.Specific ) ); 
            oStat.Caption = "Rented by"; 
            
            //  Add 3 Edit Texts
            //  Movie Code
            oItem = oForm.Items.Add( "txtCode", SAPbouiCOM.BoFormItemTypes.it_EDIT ); 
            oItem.Left = 100; 
            oItem.Top = 10; 
            oItem.AffectsFormMode = false; 
            //  Shelf
            oItem = oForm.Items.Add( "txtShelf", SAPbouiCOM.BoFormItemTypes.it_EDIT ); 
            oItem.Left = 100; 
            oItem.Top = 70; 
            oItem.AffectsFormMode = false; 
            oItem.Enabled = false; 
            
            //  Shelf
            oItem = oForm.Items.Add( "txtSpace", SAPbouiCOM.BoFormItemTypes.it_EDIT ); 
            oItem.Left = 100; 
            oItem.Top = 90; 
            oItem.AffectsFormMode = false; 
            oItem.Enabled = false; 
            
            //  Adding a rectangle
            oItem = oForm.Items.Add( "rec", SAPbouiCOM.BoFormItemTypes.it_RECTANGLE ); 
            oItem.Left = 5; 
            oItem.Top = 60; 
            oItem.Width = 180; 
            oItem.Height = 80; 
            oItem.AffectsFormMode = false; 
            
            SAPbouiCOM.ComboBox oCombo = null; 
            //  Adding a Linked Button Combobox
            oItem = oForm.Items.Add( "BPLink", SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX ); 
            oItem.Left = 100; 
            oItem.Top = 110; 
            oItem.AffectsFormMode = false; 
            oItem.LinkTo = "Linked"; 
            oItem.Enabled = false; 
            
            //  Adding a Linked Button
            oItem = oForm.Items.Add( "Linked", SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON ); 
            oItem.Left = 80; 
            oItem.Top = 110; 
            // oItem.AffectsFormMode = False
            oItem.LinkTo = "BPLink"; 
            oLinked = ( ( SAPbouiCOM.LinkedButton )( oItem.Specific ) ); 
            
            oLinked.LinkedObject = SAPbouiCOM.BoLinkedObject.lf_BusinessPartner; 
            
            //  Adding "Find Movie" button
            oItem = oForm.Items.Add( "btnFind", SAPbouiCOM.BoFormItemTypes.it_BUTTON ); 
            oItem.Left = 10; 
            oItem.Top = 35; 
            oItem.AffectsFormMode = false; 
            oButton = ( ( SAPbouiCOM.Button )( oItem.Specific ) ); 
            oButton.Type = SAPbouiCOM.BoButtonTypes.bt_Image; 
            oButton.Image = _startupPathProvider.GetStartupPath() + "search.bmp"; 
        } 
    }
}
