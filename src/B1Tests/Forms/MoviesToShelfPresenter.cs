﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IMoviesToShelfPresenter : IPresenter<IMoviesToShelfForm>
    {
        void ShowMoviesByShelf();
    }

    public class MoviesToShelfPresenter : IMoviesToShelfPresenter
    {
        IActiveFormService _activeForm;

        public delegate IMoviesToShelfPresenter Factory(IMoviesToShelfForm view);

        public MoviesToShelfPresenter(IActiveFormService activeForm)
        {
            if (activeForm == null)
                throw new ArgumentNullException("activeForm");
            _activeForm = activeForm;
        }

        // if we could somehow extend SBO_Application.Forms.ActiveForm
        // with functions returning these inputs?
        // to comply with the law of demeter
        // something like:

        // SBO_Application.ActiveForm_Mat :: unit -> SAPbouiCOM.Matrix
        // SBO_Application.ActiveForm_CmbShelfs :: unit -> SAPbouiCOM.ComboBox
        // SBO_Application.ActiveForm_CmbRented :: unit -> SAPbouiCOM.ComboBox
        // SBO_Application.ActiveForm_DBDSVids :: unit -> SAPbouiCOM.DBDataSource
        // ActiveFormItemSpecific(string itemUid)

        // inputs: SBO_Application.Forms.ActiveForm
        //   .Item("mat").Specific :: SAPbouiCOM.Matrix
        //   .Item("cmb_shelfs").Specific :: SAPbouiCOM.ComboBox
        //   .Item("cmb_rented").Specific :: SAPbouiCOM.ComboBox
        // DBDSVids = oForm.DataSources.DBDataSources.Item( "@VIDS" ); 

        // what this method does?
        // inputs: form item values provider
        // outputs: 1. resets the form datasource
        //          2. removes records excluded by form items values

        // filters the VIDS data source by:
        //   * show rented (Y), available (N), or all (A)
        //   * show movies at specific shelf num (if chosen)

        public void ShowMoviesByShelf() {
            var cmbRented = _activeForm.GetComboBoxSelectedValue("cmb_rented");
            string Rented = null; 
            
            if (cmbRented == String.Empty) { 
                Rented = "A"; 
            } 
            else { 
                switch ( cmbRented ) {
                    case "Rented":
                        Rented = "Y"; 
                        break;
                    case "Available":
                        Rented = "N"; 
                        break;
                    case "All":
                        Rented = "A"; 
                        break;
                }                
            } 
            
            // Fill the DB Datasource
            var DBDSVids = _activeForm.GetDBDataSource( "@VIDS" ); 
            DBDSVids.Query( null ); 
            
            int val;
            var cmbShelf = _activeForm.GetComboBoxSelectedValue("cmb_shelfs");            
            int ShelfNum = 0; 
            if ( int.TryParse(cmbShelf, out val) ) { //  Incase it's all
                ShelfNum = int.Parse( cmbShelf ); 
            }
            
            int i = 0; 
            
            i = 0; 
            while ( i < DBDSVids.Size ) { 
                if ( ( DBDSVids.GetValue( "U_SHELF", i ) != ( ShelfNum.ToString() ) & ( cmbShelf != "All" ) ) ) { 
                    DBDSVids.RemoveRecord( i ); 
                    i = i - 1; 
                } 
                else if ( ( DBDSVids.GetValue( "U_RENTED", i ) != Rented ) & Rented != "A" ) { 
                    DBDSVids.RemoveRecord( i ); 
                    i = i - 1; 
                } 
                i = i + 1; 
            } 
            
            var oMatrix = _activeForm.GetMatrix("mat"); 
            oMatrix.LoadFromDataSource(); 
        } 
    }
}
