﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IFindMoviesFormPresenter : IPresenter<IFindMoviesForm>
    {
        void FindMovie();
    }

    public class FindMoviesFormPresenter : IFindMoviesFormPresenter
    {
        public delegate IFindMoviesFormPresenter Factory(IFindMoviesForm view);

        IBOMessageBox _messageBox;
        IActiveFormService _activeForm;
        IFindMoviesForm _view;

        // FIXME: presenter should never reference IActiveForm
        public FindMoviesFormPresenter(IFindMoviesForm view, IBOMessageBox messageBox, IActiveFormService activeForm)
        {
            if (view == null)
                throw new ArgumentNullException("view");
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            if (activeForm == null)
                throw new ArgumentNullException("activeForm");
            _view = view;
            _messageBox = messageBox;
            _activeForm = activeForm;
        }

        // given Code in txtCode EditText control,
        // looks for it in VIDS datasource
        // if found, U_CardCode is added to 'BPLink' comboBox valid values
        //  and 'BPLink' selected value is set to record with code = U_CardCode
        // if not found, a message is displayed
        public void FindMovie()
        {
            bool isFound = false;
            var strFind = _view.GetMovieCode();

            var DBDSVids = _activeForm.GetDBDataSource("@VIDS");
            DBDSVids.Query( null ); 
            
            int i = 0; 
            
            // FIXME: linear search. any remedies?
            for (i = 0; i < DBDSVids.Size; i++) { 
                if ( ( DBDSVids.GetValue( "Code", i ).Trim() == strFind ) ) { 
                    isFound = true;
                    _view.SetShelf(Int32.Parse(DBDSVids.GetValue("U_Shelf", i).Trim()));
                    _view.SetSpace(Int32.Parse(DBDSVids.GetValue("U_Space", i).Trim()));
                    
                    //  Adding the returned BP to the Combo Box
                    string BP = null; 
                    BP = DBDSVids.GetValue( "U_CardCode", i ).Trim();
                    _view.AddBPLinkValidValue(BP);

                    // FIXME: why not exit the loop? Code is PK
                } 
            } 
            if ( !( isFound ) ) { 
                _messageBox.Notify( "Movie Code " + strFind + " was not found", 1, "Ok", "", "" ); 
            } 
        }
    }
}
