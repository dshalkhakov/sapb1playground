﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IRentMovieFormPresenter : IPresenter<IRentMovieForm>
    {
        void RentMovie();
        void ReturnMovie();
    }

    public class RentMovieFormPresenter : IRentMovieFormPresenter
    {
        public delegate IRentMovieFormPresenter Factory(IRentMovieForm view);

        IRentMovieForm _view;
        IRentMovieService _service;

        public RentMovieFormPresenter(IRentMovieForm view, IRentMovieService service)
        {
            if (view == null)
                throw new ArgumentNullException("view");
            if (service == null)
                throw new ArgumentNullException("service");
            _view = view;
            _service = service;
        }

        // given CombCode, looks if it is rented by checking VIDS user table
        // if rented, displays a message
        // if not rented, updates the database (set RENT = 'Y') and displays a success message
        // CombCode and CombCard combo boxes search for "0" key
        public void RentMovie() {             
            var MovieStr = _view.GetSelectedMovie();
            var CardStr = _view.GetSelectedCard();

            if ( _service.IsMovieRented(MovieStr) ) { 
                _view.Notify( "Movie already rented, Please choose a different one"); 
            } 
            else { 
                _service.RentMovie(MovieStr, CardStr);
                _view.Notify( "Movie Rented Successfully"); 
            }
            _view.ResetSelection();
        } 

        // given CombCode, checks if the movie is rented by querying VIDS table
        // if not rented, user is urged to choose a different movie
        // if rented, RENT set to 'N' in VIDS table for CombCode movie
        // CombCode combo box set to search for "0" key
        public void ReturnMovie() {             
            var MovieStr = _view.GetSelectedMovie();
            
            if ( _service.IsMovieRented(MovieStr) ) { 
                _view.Notify( "Movie isn't rented, Please choose a different one"); 
            }
            else { 
                _service.ReturnMovie(MovieStr);
                _view.Notify( "Movie Returned Successfully"); 
            } 
            _view.ResetSearch();
        }
    }
}
