﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IAddMovieFormPresenter : IPresenter<IAddMovieFormView>
    {
        void AddMovie();
    }

    public class AddMovieFormPresenter : IAddMovieFormPresenter
    {
        public delegate IAddMovieFormPresenter Factory(IAddMovieFormView view);

        IBOMessageBox _messageBox;
        IAddMovieFormService _addMovieService;
        IAddMovieFormView _addMovieView;

        public AddMovieFormPresenter(IBOMessageBox messageBox, IAddMovieFormService addMovieService, IAddMovieFormView addMovieView)
        {
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            if (addMovieService == null)
                throw new ArgumentNullException("addMovieService");
            if (addMovieView == null)
                throw new ArgumentNullException("addMovieView");
            _messageBox = messageBox;
            _addMovieService = addMovieService;
            _addMovieView = addMovieView;
        }

        #region Command methods

        public void AddMovie()
        {
            var strFind = _addMovieView.GetSearchMovieCode();

            if (!String.IsNullOrEmpty(strFind))
            {
                if (!_addMovieView.MovieExists(strFind))
                {
                    // Name
                    var Name = _addMovieView.GetSearchMovieName(); 
                    // Shelf
                    var Shelf = _addMovieView.GetSearchShelf(); 
                    // Space
                    var space = _addMovieView.GetSearchSpace(); 

                    // Start DI code here
                    // Public oOrder As SAPbobsCOM.Documents ' Order object
                    if ( _addMovieService.AddMovieToDatabase( strFind, Name, Shelf, space ) ) { 
                        _messageBox.Notify( "Movie Added", 1, "Ok", "", "" ); 
                    }
                    else
                    {
                        _messageBox.Notify( "Failed to add movie", 1, "Ok", "", "");
                    }
                }
                else {
                    _messageBox.Notify( "Movie with this code already exists", 1, "Ok", "", "" ); 
                }
            } 
            else { 
                _messageBox.Notify( "You must type the movie code", 1, "Ok", "", "" ); 
            } 
        } 

        #endregion
    }
}
