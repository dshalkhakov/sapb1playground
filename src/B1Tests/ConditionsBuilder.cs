﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Diagnostics;

namespace B1Tests
{
    public interface IAlias
    {
        int Int { get; }
        string Str { get; }
        float Float { get; }
        DateTime Date { get; }
        DateTime Time { get; }
        bool Bool { get; }

        bool Between(int a, int b);
        bool Between(DateTime a, DateTime b);
        bool Between(float a, float b);

        bool NotBetween(int a, int b);
        bool NotBetween(DateTime a, DateTime b);
        bool NotBetween(float a, float b);

        // Like (%)
        bool Like(string substring);

        // Not like (%)
        bool NotLike(string substring);

        // Begins with (...%)
        bool BeginsWith(string substring);

        // Not begins with (...%)
        bool NotBeginsWith(string substring);

        // Ends with (%...)
        bool EndsWith(string substring);

        // Not Ends with (%...)
        bool NotEndsWith(string substring);

        bool IsNull();
        bool NotIsNull();
    }

    public interface IAliases
    {
        IAlias this[string aliasName] { get; }
    }

    // Expr ::= Expr 'and' Expr
    //        | Expr 'or' Expr
    //        | Alias '=' Var
    //        | Alias '<>' Var
    //        | Alias '>' Var
    //        | Alias '<' Var
    //        | Alias '>=' Var
    //        | Alias '<=' Var
    // Var ::= Literal | Identifier
    // Literal ::= number | '"', alphanumeric, '"'
    // Identifier
    public class ConditionsBuilder
    {
        SAPbouiCOM.Conditions _conds;
        Stack<SAPbouiCOM.Condition> _stack;

        public ConditionsBuilder(SAPbouiCOM.Conditions conds)
        {
            //if (conds == null)
            //    throw new ArgumentNullException("conds");
            _conds = conds;
            _stack = new Stack<SAPbouiCOM.Condition>();
        }

        private void BuildMember()
        {

        }

        bool IsAliasName(Expression expr)
        {
            // FIXME train wreck
            if (!(expr is MemberExpression))
                return false;
            var member = expr as MemberExpression;
            if (member.Expression is MethodCallExpression)
            {
                var call = member.Expression as MethodCallExpression;
                if (call.Method.Name.Equals("get_Item")
                    && call.Arguments.Count == 1
                    && call.Arguments[0].NodeType == ExpressionType.Constant)
                    return true;
            }
            return false;
        }

        string GetAliasName(Expression expr)
        {
            // FIXME train wreck
            if (!IsAliasName(expr))
                throw new Exception("GetAliasName called on non-alias");
            var member = (expr as MemberExpression).Expression as MethodCallExpression;
            return (member.Arguments.First() as ConstantExpression).Value as string;
        }

        private void BuildBinary(SAPbouiCOM.Conditions conds, System.Linq.Expressions.BinaryExpression expr)
        {
            switch (expr.NodeType)
            {
                // logical operators: &&, ||
                case ExpressionType.AndAlso:
                case ExpressionType.OrElse:
                    {
                        var cond = _conds.Add();
                        cond.BracketOpenNum = _stack.Count + 1;
                        _stack.Push(cond);

                        if (expr.NodeType == ExpressionType.AndAlso)
                            cond.Relationship = SAPbouiCOM.BoConditionRelationship.cr_AND;
                        else if (expr.NodeType == ExpressionType.OrElse)
                            cond.Relationship = SAPbouiCOM.BoConditionRelationship.cr_AND;
                        else
                            throw new InvalidOperationException("Invalid logical operator");

                        // well, actually, both Left and Right subexpressions
                        // should be binary expressions
                        // any other expressions are not allowed

                        if (!(expr.Left is BinaryExpression))
                            throw new InvalidOperationException("Left subexpressions is not a binary expression");

                        BuildBinary(conds, expr.Left as BinaryExpression);

                        if (!(expr.Right is BinaryExpression))
                            throw new InvalidOperationException("Right subexpression is not a binary expression");

                        if (expr.Right is BinaryExpression)
                            BuildBinary(conds, expr.Right as BinaryExpression);
                        cond.BracketCloseNum = _stack.Count;
                        _stack.Pop();
                    }
                    break;
                // comparison operators: ==, !=, >, <, >=, <=
                case ExpressionType.Equal:
                case ExpressionType.NotEqual:
                case ExpressionType.LessThan:
                case ExpressionType.LessThanOrEqual:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                    {
                        var cond = null as SAPbouiCOM.Condition;
                        bool added = false;
                        if (_stack.Count == 0)
                        {
                            cond = _conds.Add();
                            cond.BracketOpenNum = _stack.Count + 1;
                            _stack.Push(cond);
                            added = true;
                        }
                        else
                            cond = _stack.Peek();
                        // lval: member access
                        // rval: either constant or member access
                        if (!IsAliasName(expr.Left))
                            throw new ArgumentNullException("l-val is not an alias");
                        if (!(IsAliasName(expr.Right) || expr.Right is ConstantExpression))
                            throw new ArgumentNullException("r-val must be either an alias or a constant");
                        cond.Alias = GetAliasName(expr.Left);
                        switch (expr.NodeType)
                        {
                            case ExpressionType.Equal:
                                cond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
                                break;
                            case ExpressionType.NotEqual:
                                cond.Operation = SAPbouiCOM.BoConditionOperation.co_NOT_EQUAL;
                                break;
                            case ExpressionType.LessThan:
                                cond.Operation = SAPbouiCOM.BoConditionOperation.co_LESS_THAN;
                                break;
                            case ExpressionType.LessThanOrEqual:
                                cond.Operation = SAPbouiCOM.BoConditionOperation.co_LESS_EQUAL;
                                break;
                            case ExpressionType.GreaterThan:
                                cond.Operation = SAPbouiCOM.BoConditionOperation.co_GRATER_THAN;
                                break;
                            case ExpressionType.GreaterThanOrEqual:
                                cond.Operation = SAPbouiCOM.BoConditionOperation.co_GRATER_EQUAL;
                                break;
                            default:
                                throw new InvalidOperationException("Unsupported comparison operator");
                        }

                        if (expr.Right is ConstantExpression)
                        {
                            var rval = expr.Right as ConstantExpression;
                            // any type is converted to its' string representation
                            cond.CondVal = rval.Value.ToString(); // cast is guaranteed to succeed by C# type system
                        }
                        else if (expr.Right is MemberExpression)
                        {
                            var rval = expr.Right as MemberExpression;
                            cond.ComparedAlias = rval.Member.Name;
                            cond.CompareFields = true;
                        }
                        else
                            throw new InvalidOperationException("Unexpected child of comparison expression");
                        if (!added)
                            _stack.Pop();
                    }
                    break;
                // method invocation
                // indexer expression
            }
        }

        public static void Build(SAPbouiCOM.Conditions conds, Expression<Func<IAliases, bool>> op)
        {
            if (op.Body is System.Linq.Expressions.BinaryExpression)
            {
                var builder = new ConditionsBuilder(conds);
                builder.BuildBinary(conds, op.Body as System.Linq.Expressions.BinaryExpression);
                return;
            }

            if (op.Body.NodeType == System.Linq.Expressions.ExpressionType.Constant)
                throw new InvalidOperationException("Body cannot be a constant value");
            if (op.Body is System.Linq.Expressions.UnaryExpression)
                throw new InvalidOperationException("Body cannot be an unary expression");
            throw new InvalidOperationException("Unsupported body expression");
        }
    }
}
