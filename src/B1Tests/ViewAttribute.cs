﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public class ViewAttribute : System.Attribute
    {
        public ViewAttribute(string formUID, string formTypeEx)
        {
            FormUID = formUID;
            FormTypeEx = formTypeEx;
        }

        public string FormUID { get; private set; }
        public string FormTypeEx { get; private set; }
    }
}
