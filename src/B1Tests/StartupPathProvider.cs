﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IStartupPathProvider
    {
        string GetStartupPath();
    }

    public class StartupPathProvider : IStartupPathProvider
    {
        public string GetStartupPath()
        {
            return System.Windows.Forms.Application.StartupPath;
        }
    }
}
