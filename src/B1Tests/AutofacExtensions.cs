﻿using System;
using System.Linq;
using System.Text;
using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;
using System.Reflection;

namespace B1Tests
{
    public static class AutofacExtensions
    {
        /// <summary>
        /// Register IView implementors in specified assemblies.
        /// </summary>
        /// <param name="builder">Container builder</param>
        /// <param name="formViewsAssemblies">Assemblies</param>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle>
            RegisterFormViews(this ContainerBuilder builder, params Assembly[] formViewsAssemblies)
        {
            return builder.RegisterAssemblyTypes(formViewsAssemblies)
                .Where(t => typeof(VidsSamp.IView).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .Keyed<VidsSamp.IView>(t => {
                    var attr = (VidsSamp.ViewAttribute)System.Attribute.GetCustomAttribute(t, typeof(VidsSamp.ViewAttribute));
                    if (attr == null)
                        throw new InvalidOperationException("View attr not set for type " + t.FullName);
                    return Tuple.Create(attr.FormUID, attr.FormTypeEx);
                })
                .InstancePerLifetimeScope();
        }

        /// <summary>
        /// Register classes implementing IPresenter interface
        /// in specified assemblies.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="presentersAssemblies"></param>
        /// <returns></returns>
        public static IRegistrationBuilder<object, ScanningActivatorData, DynamicRegistrationStyle>
            RegisterPresenters(this ContainerBuilder builder, params Assembly[] presentersAssemblies)
        {
            return builder.RegisterAssemblyTypes(presentersAssemblies)
                .Where(t => typeof(VidsSamp.IPresenter).IsAssignableFrom(t))
                .AsImplementedInterfaces()
                .InstancePerDependency();
        }
    }
}
