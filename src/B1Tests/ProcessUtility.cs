﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Security.Principal;
using System.Management;
using System.Windows.Forms;

namespace B1Tests
{
    public class ProcessUtility
    {
        public static void KillStrayProcesses()
        {
            Process thisProc = Process.GetCurrentProcess();
            string currentUser = thisProc.StartInfo.EnvironmentVariables["USERNAME"];
            var otherProcs = Process.GetProcesses()
                .Where(p => p.ProcessName.Equals(thisProc.ProcessName))
                .Where(p => p.Id != thisProc.Id)
                .Where(p => GetProcessOwner(p.Id).Equals(currentUser));

            try
            {
                foreach (var other in otherProcs)
                    other.Kill();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при закрытии лишних процессов " + ex.ToString());
            }
        }

        /// <summary>
        /// Возвращает UserName от которого запущен процесс по ID процесса
        /// </summary>
        /// <param name="processId">ID процесса</param>
        /// <returns></returns>
        private static string GetProcessOwner(int processId)
        {
            string query = "Select * From Win32_Process Where ProcessID = " + processId;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();

            foreach (ManagementObject obj in processList)
            {
                string[] argList = new string[] { string.Empty, string.Empty };
                int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                if (returnVal == 0)
                {
                    // return DOMAIN\user
                    return /*argList[1] + "\\" + */ argList[0];
                }
            }

            return "NO OWNER";
        }
    }
}
