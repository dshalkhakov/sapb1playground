﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IView
    {
        void Activate();
        void HandleItemEvent(ref SAPbouiCOM.ItemEvent pVal);
    }

    public interface IPresenter
    {
    }

    public interface IPresenter<TView> : IPresenter
        where TView : IView
    {
    }
}
