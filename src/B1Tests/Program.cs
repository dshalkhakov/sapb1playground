﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autofac;

namespace B1Tests
{
    static class Program
    {
        static IContainer Container;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Container = App.GetApp();

            using (var scope = Container.BeginLifetimeScope())
            {
                try
                {
                    var vids = scope.Resolve<VidsSamp.Vids>();
                    //var udt = scope.Resolve<VidsSamp.IUserDefinedTables>();
                    vids.Run();
                    //udt.Register();
                    Application.Run();
                }
                catch (Autofac.Core.Registration.ComponentNotRegisteredException e)
                {
                    ;
                }
                catch (Autofac.Core.DependencyResolutionException e)
                {
                    ;
                }
            }
        }
    }
}
