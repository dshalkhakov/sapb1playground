﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1Tests
{
    public interface IFluentFormItemConfig<T>
    {
        T AffectsFormMode(bool enable);
        T Description(string description);
        T BackColor(int color);
        T ForeColor(int color);
        T Enabled();
        T Disabled();
        T LinkTo(string link);
        T DisplayDesc(bool display);
        T WithSize(int width, int height);
        T WithPosition(int top, int left);
        T RightJustified();
    }

    public interface IFluentStaticTextConfig : IFluentFormItemConfig<IFluentStaticTextConfig>
    {

    }

    public interface IFluentButtonConfig : IFluentFormItemConfig<IFluentButtonConfig>
    {

    }

    public interface IFluentDBDataSourceConfig
    {

    }

    public abstract class FluentFormItemConfig<T> : IFluentFormItemConfig<T>
        where T : class
    {
        protected SAPbouiCOM.Item _item;

        protected FluentFormItemConfig(SAPbouiCOM.Item item)
        {
            if (item == null)
                throw new ArgumentNullException("source");
            _item = item;
        }

        public T AffectsFormMode(bool enable)
        {
            _item.AffectsFormMode = enable;
            return this as T;
        }

        public T Description(string description)
        {
            _item.Description = description;
            return this as T;
        }

        public T BackColor(int color)
        {
            _item.BackColor = color;
            return this as T;
        }

        public T ForeColor(int color)
        {
            _item.ForeColor = color;
            return this as T;
        }

        public T Enabled()
        {
            _item.Enabled = true;
            return this as T;
        }

        public T Disabled()
        {
            _item.Enabled = false;
            return this as T;
        }

        public T DisplayDesc(bool display)
        {
            _item.DisplayDesc = display;
            return this as T;
        }

        public T FontSize(int size)
        {
            _item.FontSize = size;
            return this as T;
        }

        public T WithSize(int width, int height)
        {
            _item.Width = width;
            _item.Height = height;
            return this as T;
        }

        public T WithPosition(int top, int left)
        {
            _item.Top = top;
            _item.Left = left;
            return this as T;
        }

        public T LinkTo(string link)
        {
            // TODO type safe version
            // error checks
            _item.LinkTo = link;
            return this as T;
        }

        public T RightJustified()
        {
            _item.RightJustified = true;
            return this as T;
        }

        public T TextStyle(int style)
        {
            _item.TextStyle = style;
            return this as T;
        }

        public T Hidden()
        {
            _item.Visible = false;
            return this as T;
        }

        public T Visible()
        {
            _item.Visible = true;
            return this as T;
        }
    }

    public class FluentDBDataSourceConfig : IFluentDBDataSourceConfig
    {
        public FluentDBDataSourceConfig(SAPbouiCOM.DBDataSource item)
        { }
    }

    public class FluentStaticTextConfig : FluentFormItemConfig<IFluentStaticTextConfig>, IFluentStaticTextConfig
    {
        public FluentStaticTextConfig(SAPbouiCOM.Item item) : base(item) { }

        SAPbouiCOM.StaticText GetSpecific()
        {
            return _item.Specific as SAPbouiCOM.StaticText;
        }

        public FluentStaticTextConfig Caption(string text)
        {
            GetSpecific().Caption = text;
            return this;
        }

        public FluentStaticTextConfig ClickAfter(SAPbouiCOM._IStaticTextEvents_ClickAfterEventHandler handler)
        {
            GetSpecific().ClickAfter += handler;
            return this;
        }

        public FluentStaticTextConfig ClickBefore(SAPbouiCOM._IStaticTextEvents_ClickBeforeEventHandler handler)
        {
            GetSpecific().ClickBefore += handler;
            return this;
        }

        public FluentStaticTextConfig DoubleClickAfter(SAPbouiCOM._IStaticTextEvents_DoubleClickAfterEventHandler handler)
        {
            GetSpecific().DoubleClickAfter += handler;
            return this;
        }

        public FluentStaticTextConfig DoubleClickBefore(SAPbouiCOM._IStaticTextEvents_DoubleClickBeforeEventHandler handler)
        {
            GetSpecific().DoubleClickBefore += handler;
            return this;
        }

        public FluentStaticTextConfig PressedAfter(SAPbouiCOM._IStaticTextEvents_PressedAfterEventHandler handler)
        {
            GetSpecific().PressedAfter += handler;
            return this;
        }

        public FluentStaticTextConfig PressedBefore(SAPbouiCOM._IStaticTextEvents_PressedBeforeEventHandler handler)
        {
            GetSpecific().PressedBefore += handler;
            return this;
        }
    }

    public class FluentButtonConfig : FluentFormItemConfig<IFluentButtonConfig>, IFluentButtonConfig
    {
        public FluentButtonConfig(SAPbouiCOM.Item item) : base(item) { }

        SAPbouiCOM.Button GetSpecific()
        {
            return _item.Specific as SAPbouiCOM.Button;
        }

        public FluentButtonConfig Type(SAPbouiCOM.BoButtonTypes type)
        {
            GetSpecific().Type = type;
            return this;
        }

        public FluentButtonConfig Image(string src)
        {
            GetSpecific().Image = src;
            return this;
        }

        public FluentButtonConfig Caption(string text)
        {
            GetSpecific().Caption = text;
            return this;
        }

        public FluentButtonConfig ChooseFromListUID(string uid)
        {
            // TODO type safety
            GetSpecific().ChooseFromListUID = uid;
            return this;
        }

        public FluentButtonConfig ChooseFromListAfter(SAPbouiCOM._IButtonEvents_ChooseFromListAfterEventHandler handler)
        {
            // FIXME: how to unsub on GC collection???
            GetSpecific().ChooseFromListAfter += handler;
            return this;
        }

        public FluentButtonConfig ChooseFromListBefore(SAPbouiCOM._IButtonEvents_ChooseFromListBeforeEventHandler handler)
        {
            GetSpecific().ChooseFromListBefore += handler;
            return this;
        }

        public FluentButtonConfig ClickAfter(SAPbouiCOM._IButtonEvents_ClickAfterEventHandler handler)
        {
            GetSpecific().ClickAfter += handler;
            return this;
        }

        public FluentButtonConfig ClickBefore(SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler handler)
        {
            GetSpecific().ClickBefore += handler;
            return this;
        }

        public FluentButtonConfig DoubleClickAfter(SAPbouiCOM._IButtonEvents_DoubleClickAfterEventHandler handler)
        {
            GetSpecific().DoubleClickAfter += handler;
            return this;
        }

        public FluentButtonConfig DoubleClickBefore(SAPbouiCOM._IButtonEvents_DoubleClickBeforeEventHandler handler)
        {
            GetSpecific().DoubleClickBefore += handler;
            return this;
        }
    }
}
