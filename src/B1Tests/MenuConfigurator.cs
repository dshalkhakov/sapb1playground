﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1Tests
{
    public interface IMenuConfig
    {
        IMenuConfig Image(string image);
        IMenuConfig Position(int position);
        IMenuConfig String(string s);
        void AddAsChildOf(SAPbouiCOM.Menus menus);
    }

    public interface IMenuConfigurator
    {
        IMenuConfig WithNewPopup(string uid);
        IMenuConfig WithNewSeparator(string uid);
        IMenuConfig WithNewString(string uid);
    }

    public class MenuItemConfigurator : IMenuConfig
    {
        SAPbouiCOM.MenuCreationParams _mcp = null;
        SAPbouiCOM.Application _app = null;
        VidsSamp.IStartupPathProvider _pathProvider = null;
        VidsSamp.ITopMenu _topMenu = null;

        public MenuItemConfigurator(SAPbouiCOM.Application app, VidsSamp.IStartupPathProvider pathProvider, SAPbouiCOM.MenuCreationParams mcp, VidsSamp.ITopMenu topMenu)
        {
            if (app == null)
                throw new ArgumentNullException("app");
            if (pathProvider == null)
                throw new ArgumentNullException("pathProvider");
            if (mcp == null)
                throw new ArgumentNullException("mcp");
            if (topMenu == null)
                throw new ArgumentNullException("topMenu");
            _app = app;
            _pathProvider = pathProvider;
            _mcp = mcp;
            _topMenu = topMenu;
        }

        public IMenuConfig Image(string image)
        {
            _mcp.Image = _pathProvider.GetStartupPath() + image;
            return this;
        }

        public IMenuConfig Position(int pos)
        {
            _mcp.Position = pos;
            return this;
        }

        public IMenuConfig String(string s)
        {
            _mcp.String = s;
            return this;
        }

        public void AddAsChildOf(SAPbouiCOM.Menus menus)
        {
            menus.AddEx(_mcp);
            _mcp = null;
        }
    }

    public class MenuConfigurator : IMenuConfigurator
    {
        SAPbouiCOM.Application _app;
        VidsSamp.ITopMenu _topMenu;
        VidsSamp.IStartupPathProvider _pathProvider;
        Func<SAPbouiCOM.MenuCreationParams, IMenuConfig> _menuItemConfigurator;

        SAPbouiCOM.MenuCreationParams GetMenuCreationParams(string uid)
        {
            var mcp = _app.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
                as SAPbouiCOM.MenuCreationParams;
            mcp.UniqueID = uid;
            return mcp;
        }

        public MenuConfigurator(SAPbouiCOM.Application app, VidsSamp.ITopMenu topMenu, VidsSamp.IStartupPathProvider pathProvider, Func<SAPbouiCOM.MenuCreationParams, IMenuConfig> menuItemConfigurator)
        {
            if (app == null)
                throw new ArgumentNullException("app");
            if (topMenu == null)
                throw new ArgumentNullException("topMenu");
            if (pathProvider == null)
                throw new ArgumentNullException("pathProvider");
            if (menuItemConfigurator == null)
                throw new ArgumentNullException("menuItemConfigurator");
            _app = app;
            _topMenu = topMenu;
            _pathProvider = pathProvider;
            _menuItemConfigurator = menuItemConfigurator;
        }

        public IMenuConfig WithNewPopup(string uid)
        {
            var mcp = GetMenuCreationParams(uid);
            mcp.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
            return _menuItemConfigurator(mcp);
        }

        public IMenuConfig WithNewSeparator(string uid)
        {
            var mcp = GetMenuCreationParams(uid);
            mcp.Type = SAPbouiCOM.BoMenuType.mt_SEPERATOR;
            return _menuItemConfigurator(mcp);
        }

        public IMenuConfig WithNewString(string uid)
        {
            var mcp = GetMenuCreationParams(uid);
            mcp.Type = SAPbouiCOM.BoMenuType.mt_STRING;
            return _menuItemConfigurator(mcp);
        }
    }
}
