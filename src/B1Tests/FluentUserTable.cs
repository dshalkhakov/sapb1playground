﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1Tests
{
    public interface IFluentUserTableFieldMD
    {
        IFluentUserTableFieldMD Description(string description);
        IFluentUserTableFieldMD EditSize(int size);
        IFluentUserTableFieldMD Mandatory();
        IFluentUserTableFieldMD Type(SAPbobsCOM.BoFieldTypes type);
        IFluentUserTableFieldMD SubType(SAPbobsCOM.BoFldSubTypes subType);
        int Add();
    }

    public class FluentUserTableFieldMD : IFluentUserTableFieldMD
    {
        SAPbobsCOM.UserFieldsMD _field;

        public FluentUserTableFieldMD(SAPbobsCOM.UserFieldsMD field)
        {
            if (field == null)
                throw new ArgumentNullException("field");
            _field = field;
        }

        public IFluentUserTableFieldMD Type(SAPbobsCOM.BoFieldTypes type)
        {
            _field.Type = type;
            return this;
        }

        public IFluentUserTableFieldMD SubType(SAPbobsCOM.BoFldSubTypes subType)
        {
            _field.SubType = subType;
            return this;
        }

        internal IFluentUserTableFieldMD TableName(string tableName)
        {
            _field.TableName = "@" + tableName;
            return this;
        }

        internal IFluentUserTableFieldMD Name(string name)
        {
            _field.Name = name;
            return this;
        }

        public IFluentUserTableFieldMD Description(string description)
        {
            _field.Description = description;
            return this;
        }

        public IFluentUserTableFieldMD EditSize(int size)
        {
            _field.EditSize = size;
            return this;
        }

        public IFluentUserTableFieldMD Mandatory()
        {
            _field.Mandatory = SAPbobsCOM.BoYesNoEnum.tYES;
            return this;
        }

        public int Add()
        {
            // TODO check return value
            return _field.Add();
        }
    }

    public interface IFluentUserTableMD
    {
        IFluentUserTableMD TableDescription(string description);
        IFluentUserTableMD TableType(SAPbobsCOM.BoUTBTableType type);
        IFluentUserTableFieldMD WithField(string name);
        IFluentUserTableFieldMD WithDateField(string name);
        IFluentUserTableFieldMD WithTimeField(string name);
        int Add();
    }

    public interface IFluentUserTableMDBuilder
    {
        IFluentUserTableMD WithNewDocumentTable(string tableName);
        IFluentUserTableMD WithNewDocumentLinesTable(string tableName);
        IFluentUserTableMD WithNewMasterDataTable(string tableName);
        IFluentUserTableMD WithNewMasterDataLinesTable(string tableName);
    }

    public class FluentUserTableMDBuilder : IFluentUserTableMDBuilder
    {
        SAPbobsCOM.Company _company;

        public FluentUserTableMDBuilder(SAPbobsCOM.Company company)
        {
            if (company == null)
                throw new ArgumentNullException("company");
            _company = company;
        }

        public IFluentUserTableMD WithNewMasterDataTable(string tableName)
        {
            var UDT = ((SAPbobsCOM.UserTablesMD)(_company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)));
            UDT.TableType = SAPbobsCOM.BoUTBTableType.bott_MasterData;
            return new FluentUserTableMD(_company, UDT);
        }

        public IFluentUserTableMD WithNewMasterDataLinesTable(string tableName)
        {
            var UDT = ((SAPbobsCOM.UserTablesMD)(_company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)));
            UDT.TableType = SAPbobsCOM.BoUTBTableType.bott_MasterDataLines;
            return new FluentUserTableMD(_company, UDT);
        }

        public IFluentUserTableMD WithNewDocumentTable(string tableName)
        {
            var UDT = ((SAPbobsCOM.UserTablesMD)(_company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)));
            UDT.TableType = SAPbobsCOM.BoUTBTableType.bott_Document;
            return new FluentUserTableMD(_company, UDT);
        }

        public IFluentUserTableMD WithNewDocumentLinesTable(string tableName)
        {
            var UDT = ((SAPbobsCOM.UserTablesMD)(_company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables)));
            UDT.TableType = SAPbobsCOM.BoUTBTableType.bott_DocumentLines;
            return new FluentUserTableMD(_company, UDT);
        }

    }

    public class FluentUserTableMD : IFluentUserTableMD
    {
        SAPbobsCOM.Company _company;
        SAPbobsCOM.UserTablesMD _userTable;

        public FluentUserTableMD(SAPbobsCOM.Company company, SAPbobsCOM.UserTablesMD userTable)
        {
            if (company == null)
                throw new ArgumentNullException("company");
            if (userTable == null)
                throw new ArgumentNullException("userTable");
            _company = company;
            _userTable = userTable;
        }

        internal IFluentUserTableMD TableName(string tableName)
        {
            _userTable.TableName = tableName;
            return this;
        }

        public IFluentUserTableMD TableDescription(string description)
        {
            _userTable.TableDescription = description;
            return this;
        }

        public IFluentUserTableMD TableType(SAPbobsCOM.BoUTBTableType type)
        {
            _userTable.TableType = type;
            return this;
        }

        public IFluentUserTableFieldMD WithField(string name)
        {
            var field = ((SAPbobsCOM.UserFieldsMD)(_company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)));
            field.Name = name;
            field.TableName = _userTable.TableName;
            return new FluentUserTableFieldMD(field);
        }

        public IFluentUserTableFieldMD WithDateField(string name)
        {
            var field = ((SAPbobsCOM.UserFieldsMD)(_company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)));
            field.Name = name;
            field.TableName = _userTable.TableName;
            field.Type = SAPbobsCOM.BoFieldTypes.db_Date;
            return new FluentUserTableFieldMD(field);
        }

        public IFluentUserTableFieldMD WithTimeField(string name)
        {
            var field = ((SAPbobsCOM.UserFieldsMD)(_company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields)));
            field.Name = name;
            field.TableName = _userTable.TableName;
            field.Type = SAPbobsCOM.BoFieldTypes.db_Date;
            field.SubType = SAPbobsCOM.BoFldSubTypes.st_Time;
            return new FluentUserTableFieldMD(field);
        }

        public int Add()
        {
            return _userTable.Add();
        }
    }
}
