﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IActiveFormService
    {
        SAPbouiCOM.Matrix GetMatrix(string uid);
        SAPbouiCOM.ComboBox GetComboBox(string uid);
        string GetComboBoxSelectedValue(string uid);
        string GetComboBoxSelectedDescription(string uid);

        SAPbouiCOM.DBDataSource GetDBDataSource(string tableName);
        SAPbouiCOM.EditText GetEditText(string uid);
        string GetEditTextString(string uid);
        void SetEditTextString(string uid, string s);
    }

    public class ActiveFormService : IActiveFormService
    {
        SAPbouiCOM.Application _app;

        public ActiveFormService(SAPbouiCOM.Application app)
        {
            if (app == null)
                throw new ArgumentNullException("app");
            _app = app;
        }

        public SAPbouiCOM.Matrix GetMatrix(string uid)
        {
            return _app.Forms.ActiveForm.Items.Item(uid).Specific
                as SAPbouiCOM.Matrix;
        }

        public SAPbouiCOM.ComboBox GetComboBox(string uid)
        {
            return _app.Forms.ActiveForm.Items.Item(uid).Specific
                as SAPbouiCOM.ComboBox;
        }

        public string GetComboBoxSelectedValue(string uid)
        {
            var cmb = GetComboBox(uid);
            if (cmb == null)
                return String.Empty; // or null??
            if (cmb.Selected == null)
                return String.Empty; // or null??
            return cmb.Selected.Value;
        }

        public string GetComboBoxSelectedDescription(string uid)
        {
            var cmb = GetComboBox(uid);
            if (cmb == null)
                return String.Empty; // or null??
            if (cmb.Selected == null)
                return String.Empty; // or null??
            return cmb.Selected.Description;
        }

        public SAPbouiCOM.DBDataSource GetDBDataSource(string tableName)
        {
            return _app.Forms.ActiveForm.DataSources.DBDataSources.Item(tableName);
        }

        public SAPbouiCOM.EditText GetEditText(string uid)
        {
            return _app.Forms.ActiveForm.Items.Item(uid).Specific
                as SAPbouiCOM.EditText;
        }

        public string GetEditTextString(string uid)
        {
            var editText = GetEditText(uid);
            if (editText == null)
                return String.Empty; // or null?
            return editText.String;
        }

        public void SetEditTextString(string uid, string s)
        {
            var editText = GetEditText(uid);
            if (editText == null)
                return; // FIXME: throw exception?
            editText.String = s;
        }
    }
}
