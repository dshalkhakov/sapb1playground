﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IFileLoader
    {
        XmlDocument LoadXMLFromStartupPath(string fileName);
    }

    public class FileLoader : IFileLoader
    {
        IStartupPathProvider _pathProvider;

        public FileLoader(IStartupPathProvider pathProvider)
        {
            if (pathProvider == null)
                throw new ArgumentNullException("pathProvider");
            _pathProvider = pathProvider;
        }

        public XmlDocument LoadXMLFromStartupPath(string fileName)
        {
            var oXmlDoc = new System.Xml.XmlDocument();
            var sPath = _pathProvider.GetStartupPath();
            
            oXmlDoc.Load( sPath + "\\" + fileName ); 

            return oXmlDoc;
        }
    }

    public interface IFormService
    {
        void WithForm(string uid, Action<SAPbouiCOM.Form> whenExists, Action<SAPbouiCOM.FormCreationParams> creation, Action<SAPbouiCOM.Form> configure);
    }

    public class FormService : IFormService
    {
        SAPbouiCOM.Application _app;

        public FormService(SAPbouiCOM.Application app)
        {
            if (app == null)
                throw new ArgumentNullException("app");
            _app = app;
        }

        public void WithForm(string uid, Action<SAPbouiCOM.Form> whenExists, Action<SAPbouiCOM.FormCreationParams> whenNotExists, Action<SAPbouiCOM.Form> configure)
        {
            if (whenExists == null)
                throw new ArgumentNullException("whenExists");
            if (whenNotExists == null)
                throw new ArgumentNullException("whenNotExists");
            if (configure == null)
                throw new ArgumentNullException("configure");

            SAPbouiCOM.Form form;
            try
            {
                form = _app.Forms.Item(uid);
            }
            catch (Exception ex)
            {
                var fcp = _app.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams)
                    as SAPbouiCOM.FormCreationParams;
                fcp.UniqueID = uid;
                whenNotExists(fcp);
                form = _app.Forms.AddEx(fcp);
                configure(form);
            }
            whenExists(form);
        }
    }

    public interface IBOMessageBox
    {
        void Notify(string s);
        void Notify(string s, int defaultBtn);
        void Notify(string s, int defaultBtn, string bt1nCaption);
        void Notify(string s, int defaultBtn, string btn1Caption, string btn2Caption);
        void Notify(string s, int defaultBtn, string btn1Caption, string btn2Caption, string btn3Caption);
    }

    public class BOMessageBox : IBOMessageBox
    {
        SAPbouiCOM.Application _app;

        public BOMessageBox(SAPbouiCOM.Application app)
        {
            if (app == null)
                throw new ArgumentNullException("app");
            _app = app;
        }

        void NotifyImpl(string s, int defaultBtn = 1, string btn1Caption = "Ok", string btn2Caption = "", string btn3Caption = "")
        {
            _app.MessageBox(s, defaultBtn, btn1Caption, btn2Caption, btn3Caption);
        }

        public void Notify(string s)
        {
            NotifyImpl(s);
        }

        public void Notify(string s, int defaultBtn)
        {
            NotifyImpl(s, defaultBtn);
        }

        public void Notify(string s, int defaultBtn, string btn1Caption)
        {
            NotifyImpl(s, defaultBtn, btn1Caption);
        }
        public void Notify(string s, int defaultBtn, string btn1Caption, string btn2Caption)
        {
            NotifyImpl(s, defaultBtn, btn1Caption, btn2Caption);
        }
        public void Notify(string s, int defaultBtn, string btn1Caption, string btn2Caption, string btn3Caption)
        {
            NotifyImpl(s, defaultBtn, btn1Caption, btn2Caption, btn3Caption);
        }
    }

    public interface ITopMenu
    {
        SAPbouiCOM.Menus GetTopMenu();
        SAPbouiCOM.MenuItem FindMenuItem(string id);
        SAPbouiCOM.Menus GetSubMenus(string id);
        int GetSubMenusCount(string uid);

        // FIXME: doesn't really fit here
        void ActivateMenuItem(string uid);
    }

    public class BOTopMenu : ITopMenu
    {
        SAPbouiCOM.Application _app;
        public BOTopMenu(SAPbouiCOM.Application app)
        {
            if (app == null)
                throw new ArgumentNullException("app");
            _app = app;
        }
        public void ActivateMenuItem(string uid)
        {
            _app.ActivateMenuItem(uid);
        }
        // FIXME violates LoD
        public SAPbouiCOM.Menus GetTopMenu()
        {
            return _app.Menus;
        }

        // FIXME violates LoD
        public SAPbouiCOM.MenuItem FindMenuItem(string id)
        {
            return _app.Menus.Item(id);
        }

        public SAPbouiCOM.Menus GetSubMenus(string id)
        {
            var item = FindMenuItem(id);
            if (item == null)
                return null;
            return item.SubMenus;
        }

        public int GetSubMenusCount(string id)
        {
            var item = FindMenuItem(id);
            if (item == null)
                return 0;
            return item.SubMenus.Count;
        }
    }
}
