﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1Tests
{
    public interface IFluentFormConfig
    {
        IFluentFormConfig AutoManage(bool enable);
        IFluentFormConfig WithClientSize(int clientWidth, int clientHeight);
        IFluentFormConfig WithMaxSize(int width, int height);
        IFluentFormConfig WithPosition(int top, int left);
        IFluentFormConfig Title(string title);

        IFluentDBDataSourceConfig WithDBDataSource(string tableName);
        IFluentButtonConfig WithButton(string uid);
        IFluentStaticTextConfig WithStaticText(string uid);
    }

    // allows one to configure forms in fluent way
    // mainly, to comply with the LoD
    public class FluentFormConfig : IFluentFormConfig
    {
        SAPbouiCOM.Form _form;

        private FluentFormConfig(SAPbouiCOM.Form form)
        {
            if (form == null)
                throw new ArgumentNullException("form");
            _form = form;
        }

        static public IFluentFormConfig Configure(SAPbouiCOM.Form form)
        {
            return new FluentFormConfig(form);
        }

        public IFluentFormConfig AutoManage(bool enable)
        {
            _form.AutoManaged = enable;
            return this;
        }

        public IFluentFormConfig WithClientSize(int clientWidth, int clientHeight)
        {
            _form.ClientWidth = clientWidth;
            _form.ClientHeight = clientHeight;
            return this;
        }

        public IFluentFormConfig WithMaxSize(int width, int height)
        {
            _form.MaxWidth = width;
            _form.MaxHeight = height;
            return this;
        }

        public IFluentFormConfig WithPosition(int top, int left)
        {
            _form.Top = top;
            _form.Left = left;
            return this;
        }

        public IFluentFormConfig Title(string title)
        {
            _form.Title = title;
            return this;
        }

        public IFluentDBDataSourceConfig WithDBDataSource(string tableName)
        {
            var item = _form.DataSources.DBDataSources.Add(tableName);
            return new FluentDBDataSourceConfig(item);
        }

        public IFluentButtonConfig WithButton(string uid)
        {
            var item = _form.Items.Add(uid, SAPbouiCOM.BoFormItemTypes.it_BUTTON);
            var conf = new FluentButtonConfig(item);
            return conf;
        }

        public IFluentStaticTextConfig WithStaticText(string uid)
        {
            var item = _form.Items.Add(uid, SAPbouiCOM.BoFormItemTypes.it_STATIC);
            return new FluentStaticTextConfig(item);
        }
    }
}
