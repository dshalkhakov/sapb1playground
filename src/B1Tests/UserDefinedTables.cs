﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VidsSamp
{
    public interface IUserDefinedTables
    {
        void Register();
    }

    public class UserDefinedTables : IUserDefinedTables
    {
        IUserTableService _userTable;
        B1Tests.IFluentUserTableMDBuilder _userTableBuilder;
        SAPbobsCOM.Company _company;
        IBOMessageBox _messageBox;

        public UserDefinedTables(SAPbobsCOM.Company company, IUserTableService userTable, B1Tests.IFluentUserTableMDBuilder userTableBuilder, IBOMessageBox messageBox)
        {
            if (company == null)
                throw new ArgumentNullException("company");
            if (userTable == null)
                throw new ArgumentNullException("userTable");
            if (userTableBuilder == null)
                throw new ArgumentNullException("userTableBuilder");
            if (messageBox == null)
                throw new ArgumentNullException("messageBox");
            _userTable = userTable;
            _userTableBuilder = userTableBuilder;
            _company = company;
            _messageBox = messageBox;
        }

        public void Register()
        {
            if (!_userTable.UserTableExists("VIDS"))
            {
                RegisterTable();
                RegisterFields();
            }
        }

        private void RegisterTable() { 
            //  User tables MetaData object
            SAPbobsCOM.UserTablesMD oUserTablesMD = null; 
            
            //  Init the MetaData object
            oUserTablesMD = ( ( SAPbobsCOM.UserTablesMD )( _company.GetBusinessObject( SAPbobsCOM.BoObjectTypes.oUserTables ) ) ); 

            //  set the 2 mandatory fields
            oUserTablesMD.TableName = "VIDS"; 
            oUserTablesMD.TableDescription = "Video Managment"; 
            var lRetCode = oUserTablesMD.Add(); 

            // check for errors in the process
            if ( lRetCode != 0 ) { 
                string lErrCode = "";
                string sErrMsg;
                int transTemp5 = System.Convert.ToInt32( lErrCode ); 
                _company.GetLastError( out transTemp5, out sErrMsg ); 
                _messageBox.Notify( sErrMsg, 1 ); 
            }
            else {
                _messageBox.Notify( "Table: " + oUserTablesMD.TableName + " was added successfully", 1 ); 
            } 
        } 
        
        
        private void RegisterFields() { 
            //  A User Fields object
            SAPbobsCOM.UserFieldsMD oUserFieldsMD = null; 
            
            //  Remember to free occupied resources
            GC.Collect(); 
            
            //  Init the user fields object
            oUserFieldsMD = ( ( SAPbobsCOM.UserFieldsMD )( _company.GetBusinessObject( SAPbobsCOM.BoObjectTypes.oUserFields ) ) ); 
            
            // ******************************
            //  Adding the "SHELF" field
            // ******************************
            //  Setting the Field's mandatory properties
            oUserFieldsMD.TableName = "@VIDS"; 
            oUserFieldsMD.Name = "SHELF"; 
            oUserFieldsMD.Description = "Shelf Number"; 
            oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Numeric; 
            oUserFieldsMD.EditSize = 2; 
            // Adding the Field to the Table
            int lRetCode = oUserFieldsMD.Add(); 
            
            // Check for errors
            if ( lRetCode != 0 ) { 
                int transTemp4; 
                string sErrMsg = "";
                _company.GetLastError( out transTemp4, out sErrMsg ); 
                _messageBox.Notify( sErrMsg, 1 ); 
            } 
            else { 
                _messageBox.Notify( "Field: '" + oUserFieldsMD.Name + "' was added successfuly to " + oUserFieldsMD.TableName + " Table", 1 ); 
            }
            
            // ******************************
            //  Adding the "SPACE" field
            // ******************************
            //  Setting the Field's mandatory properties
            oUserFieldsMD.TableName = "@VIDS"; 
            oUserFieldsMD.Name = "SPACE"; 
            oUserFieldsMD.Description = "SPACE Number"; 
            oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Numeric; 
            oUserFieldsMD.EditSize = 3; 
            // Adding the Field to the Table
            lRetCode = oUserFieldsMD.Add(); 
            
            // Check for errors
            if ( lRetCode != 0 ) { 
                int lErrCode = 0;
                string sErrMsg;
                int transTemp3 = System.Convert.ToInt32( lErrCode ); 
                _company.GetLastError( out transTemp3, out sErrMsg ); 
                _messageBox.Notify( sErrMsg, 1 ); 
            } 
            else { 
                _messageBox.Notify( "Field: '" + oUserFieldsMD.Name + "' was added successfuly to " + oUserFieldsMD.TableName + " Table", 1 ); 
            } 
            // ******************************
            //  Adding the "RENTED" field
            // ******************************
            //  Setting the Field's mandatory properties
            oUserFieldsMD.TableName = "@VIDS"; 
            oUserFieldsMD.Name = "RENTED"; 
            oUserFieldsMD.Description = "Rented/Available"; 
            oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha; 
            oUserFieldsMD.EditSize = 1; 
            // Adding the Field to the Table
            lRetCode = oUserFieldsMD.Add(); 
            
            // Check for errors
            if ( lRetCode != 0 ) { 
                int lErrCode = 0;
                string sErrMsg;
                int transTemp2 = System.Convert.ToInt32( lErrCode ); 
                _company.GetLastError( out transTemp2, out sErrMsg ); 
                _messageBox.Notify( sErrMsg, 1 ); 
            } 
            else { 
                _messageBox.Notify( "Field: '" + oUserFieldsMD.Name + "' was added successfuly to " + oUserFieldsMD.TableName + " Table", 1 ); 
            } 
            // ******************************
            //  Adding the "CARDCODE" field
            // ******************************
            //  Setting the Field's mandatory properties
            oUserFieldsMD.TableName = "@VIDS"; 
            oUserFieldsMD.Name = "CARDCODE"; 
            oUserFieldsMD.Description = "Card Code"; 
            oUserFieldsMD.Type = SAPbobsCOM.BoFieldTypes.db_Alpha; 
            oUserFieldsMD.EditSize = 15; 
            // Adding the Field to the Table
            lRetCode = oUserFieldsMD.Add(); 
            
            // Check for errors
            if ( lRetCode != 0 ) { 
                int lErrCode = 0;
                string sErrMsg;
                int transTemp1 = System.Convert.ToInt32( lErrCode ); 
                _company.GetLastError( out transTemp1, out sErrMsg ); 
                _messageBox.Notify( sErrMsg, 1 ); 
            } 
            else { 
                _messageBox.Notify( "Field: '" + oUserFieldsMD.Name + "' was added successfuly to " + oUserFieldsMD.TableName + " Table", 1 ); 
            } 
        } 

        private void CmdAddRecord_Click( System.Object sender, System.EventArgs e ) { 
            SAPbobsCOM.UserTable oUserTable = null; 
            
            // set the object with the requested table
            oUserTable = _company.UserTables.Item( "VIDS" ); 
            
            // set the two default fields 
            oUserTable.Code = System.Convert.ToString( 15 ); 
            oUserTable.Name = "Speed 1"; 
            
            // set the user fields
            // Shelf Number
            oUserTable.UserFields.Fields.Item( "U_SHELF" ).Value = 5; 
            // Space Number
            oUserTable.UserFields.Fields.Item( "U_SPACE" ).Value = 2; 
            // Rented
            oUserTable.UserFields.Fields.Item( "U_RENTED" ).Value = "Y"; 
            // CardCode
            oUserTable.UserFields.Fields.Item( "U_CARDCODE" ).Value = "C50003"; 
            
            // add the data to the Data base
            oUserTable.Add(); 
            
            // Check for errors
            int lErrCode = 0;
            string sErrMsg;
            int transTemp0 = System.Convert.ToInt32( lErrCode ); 
            _company.GetLastError( out transTemp0, out sErrMsg ); 
            if ( lErrCode != 0 ) { 
                _messageBox.Notify( sErrMsg, 1 ); 
            } 
            else { 
                _messageBox.Notify( "Record saved in DB successfuly", 1 ); 
            } 
            
            oUserTable = null; 
        }  
    }
}
