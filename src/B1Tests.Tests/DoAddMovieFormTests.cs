﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using VidsSamp;

namespace B1Tests.Tests
{
    public class DoAddMovieFormTests
    {
        [Fact]
        public void DoAddMovie_EmptyMovieCode_UserNotified()
        {
            var addMovieView = Mock.Of<IAddMovieFormView>(f =>
                f.GetSearchMovieCode() == "");
            var notify = new Mock<IBOMessageBox>();
            var addMovieM = new Mock<IAddMovieFormService>();

            var doAddMovie = new AddMovieFormPresenter(notify.Object, addMovieM.Object, addMovieView);
            doAddMovie.AddMovie();

            notify.Verify(n => n.Notify("You must type the movie code", 1, "Ok", "", ""));
        }

        [Fact]
        public void DoAddMovie_EmptyMovieCode_MovieNotAdded()
        {
            var addMovieView = new Mock<IAddMovieFormView>();
            var notify = new Mock<IBOMessageBox>();
            var addMovieM = new Mock<IAddMovieFormService>();

            addMovieView.Setup(f => f.GetSearchMovieCode())
                .Returns<string>(null);
            bool addMovieCalled = false;
            addMovieM.Setup(f => f.AddMovieToDatabase(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Callback(() => { addMovieCalled = true; });

            var doAddMovie = new AddMovieFormPresenter(notify.Object, addMovieM.Object, addMovieView.Object);
            doAddMovie.AddMovie();

            Assert.False(addMovieCalled);
        }

        [Fact]
        public void DoAddMovie_DuplicateMovieCodeSupplied_UserNotified()
        {
            var addMovieView = new Mock<IAddMovieFormView>();
            var notify = new Mock<IBOMessageBox>();
            var addMovieM = new Mock<IAddMovieFormService>();

            addMovieView.Setup(f => f.GetSearchMovieCode())
                .Returns("foo");
            addMovieView.Setup(f => f.MovieExists(It.IsAny<string>()))
                .Returns(true);

            notify.Setup(n => n.Notify("Movie with this code already exists", 1, "Ok", "", ""));

            var doAddMovie = new AddMovieFormPresenter(notify.Object, addMovieM.Object, addMovieView.Object);

            doAddMovie.AddMovie();

            notify.Verify();
        }

        [Fact]
        public void DoAddMovie_DuplicateMovieCodeSupplied_UserNotified_MovieNotAdded()
        {
            var addMovieView = new Mock<IAddMovieFormView>();
            var notify = new Mock<IBOMessageBox>();
            var addMovieM = new Mock<IAddMovieFormService>();

            var doAddMovie = new AddMovieFormPresenter(notify.Object, addMovieM.Object, addMovieView.Object);
            addMovieView.Setup(f => f.GetSearchMovieCode())
                .Returns("foo");
            addMovieView.Setup(f => f.MovieExists(It.IsAny<string>()))
                .Returns(true);
            var addMovieCalled = false;
            addMovieM.Setup(f => f.AddMovieToDatabase(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Callback(() => { addMovieCalled = true; });
            doAddMovie.AddMovie();

            Assert.False(addMovieCalled);
        }

        [Fact]
        public void DoAddMovie_MovieCodeSupplied_MovieAddedSuccessfully()
        {
            var addMovieView = Mock.Of<IAddMovieFormView>(f =>
                f.GetSearchMovieCode() == "foo"
                    && f.MovieExists(It.IsAny<string>()) == false
                    && f.GetSearchMovieName() == "BTTF II"
                    && f.GetSearchShelf() == 10
                    && f.GetSearchSpace() == 5);
            var notify = new Mock<IBOMessageBox>();
            var addMovieM = new Mock<IAddMovieFormService>();

            var presenter = new AddMovieFormPresenter(notify.Object, addMovieM.Object, addMovieView);

            presenter.AddMovie();
            addMovieM.Verify(f => f.AddMovieToDatabase("foo", "BTTF II", 10, 5));
        }

        [Fact]
        public void DoAddMovie_MovieCodeSupplied_MovieAddedSuccessfuly_UserNotified()
        {
            var addMovieVM = Mock.Of<IAddMovieFormView>(f =>
                f.GetSearchMovieCode() == "foo"
                && f.MovieExists(It.IsAny<string>()) == false);
            var notify = new Mock<IBOMessageBox>();
            var addMovieM = new Mock<IAddMovieFormService>();

            notify.Setup(n => n.Notify("Movie Added", 1, "Ok", "", ""));

            var presenter = new AddMovieFormPresenter(notify.Object, addMovieM.Object, addMovieVM);

            presenter.AddMovie();

            notify.Verify();
        }

        [Fact]
        public void DoAddMovie_MovieCodeSupplied_MovieAddFailed_UserNotified()
        {
            var addMovieView = Mock.Of<IAddMovieFormView>(f =>
                f.GetSearchMovieCode() == "foo"
                && f.MovieExists(It.IsAny<string>()) == false);
            var notify = new Mock<IBOMessageBox>();
            var addMovieM = Mock.Of<IAddMovieFormService>(f =>
                f.AddMovieToDatabase(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>())
                    == false);

            notify.Setup(n => n.Notify("Failed to add movie", 1, "Ok", "", "")).Verifiable();

            var presenter = new AddMovieFormPresenter(notify.Object, addMovieM, addMovieView);

            presenter.AddMovie();

            notify.Verify();
        }
    }
}
