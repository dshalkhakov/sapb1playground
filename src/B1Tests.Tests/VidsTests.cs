﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using VidsSamp;

namespace B1Tests.Tests
{
    public class VidsFixture
    {
        [Fact]
        public void OnInit_TopMenuDoesNotExist_TopMenuItemsAdded()
        {
            var appDummy = Mock.Of<SAPbouiCOM.Application>();
            var companyDummy = Mock.Of<SAPbobsCOM.Company>();
            var messageBoxDummy = Mock.Of<IBOMessageBox>();
            var menuItemStub = Mock.Of<SAPbouiCOM.MenuItem>();
            var menuConfigurator = new Mock<IMenuConfigurator>();
            var index = Mock.Of<Autofac.Features.Indexed.IIndex<Tuple<string, string>, VidsSamp.IView>>();
            menuConfigurator.SetupAllProperties();

            var topMenuStub = Mock.Of<ITopMenu>(tm => tm.FindMenuItem("43520") == menuItemStub);

            VidsSamp.Vids vids = new VidsSamp.Vids(appDummy,
                companyDummy,
                messageBoxDummy,
                topMenuStub,
                menuConfigurator.Object, index);

            vids.Run();

//            Assert.False(true);
        }

        [Fact]
        public void OnInit_TopMenuDoesExist_NothingIsAdded()
        {
            Assert.False(true);
        }
    }
}
