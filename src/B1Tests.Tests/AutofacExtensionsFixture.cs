﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Autofac;
using B1Tests;

namespace B1Tests.Tests
{
    public interface ITestFormView : VidsSamp.IView
    {
        ITestFormPresenter Presenter { get; }
    }

    [VidsSamp.View("testUID", "")]
    public class TestFormView : ITestFormView
    {
        TestFormPresenter.Factory _presenterFactory;

        public TestFormView(TestFormPresenter.Factory presenterFactory)
        {
            if (presenterFactory == null)
                throw new ArgumentNullException("presenterFactory");
            _presenterFactory = presenterFactory;
        }

        ITestFormPresenter _presenter;

        public void Activate()
        {
            if (_presenter == null)
                _presenter = _presenterFactory(this, true);
        }

        public void HandleItemEvent(ref SAPbouiCOM.ItemEvent ev) { }

        public ITestFormPresenter Presenter { get { return _presenter; } }
    }

    public interface ITestFormPresenter : VidsSamp.IPresenter<ITestFormView>
    {

    }

    public interface ITestFormService { }

    public class TestFormService : ITestFormService
    {
        public TestFormService() { }
    }

    public class TestFormPresenter : ITestFormPresenter
    {
        public delegate TestFormPresenter Factory(ITestFormView view, bool someParm);

        public ITestFormView View { get; set; }
        public ITestFormService Service { get; set; }
        public bool SomeParm { get; set; }

        public TestFormPresenter(ITestFormView view, bool someParm, ITestFormService service)
        {
            if (view == null)
                throw new ArgumentNullException("view");
            if (service == null)
                throw new ArgumentNullException("service");
            View = view;
            Service = service;
            SomeParm = someParm;
        }
    }

    public class AutofacExtensionsFixture
    {
        [Fact]
        public void ViewWithoutAttrNotRegistered()
        {
            var builder = new ContainerBuilder();
            
            builder.RegisterFormViews(GetType().Assembly);
        }

        [Fact]
        public void FactoryWorks()
        {
            var builder = new ContainerBuilder();

            builder.RegisterFormViews(GetType().Assembly);
            builder.RegisterType<TestFormPresenter>();
            builder.RegisterType<TestFormService>()
                .AsImplementedInterfaces();

            var container = builder.Build();
            var view = container.Resolve<ITestFormView>();

            Assert.NotNull(view);
            Assert.Null(view.Presenter);
            view.Activate();
            Assert.NotNull(view.Presenter);
            Assert.True((view.Presenter as TestFormPresenter).SomeParm);
            Assert.True((view.Presenter as TestFormPresenter).View == view);
        }

        [Fact]
        public void RegistersFormViews()
        {
            var builder = new ContainerBuilder();
            builder.RegisterFormViews(GetType().Assembly);

            var container = builder.Build();

            Assert.True(container.IsRegistered<ITestFormView>());
        }

        [Fact]
        public void RegistersFormPresenters()
        {
            var builder = new ContainerBuilder();
            builder.RegisterPresenters(GetType().Assembly);

            var container = builder.Build();

            Assert.True(container.IsRegistered<ITestFormPresenter>());
        }
    }
}
