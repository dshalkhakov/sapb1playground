﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using VidsSamp;
using Moq;

namespace B1Tests.Tests
{
    public class FormServiceTests
    {
        const string UID = "uid";

        Mock<SAPbouiCOM.Application> GetAppStub()
        {
            var fcpStub = new Mock<SAPbouiCOM.FormCreationParams>();
            fcpStub.SetupProperty(fcp => fcp.UniqueID);

            var appStub = new Mock<SAPbouiCOM.Application>(MockBehavior.Strict);

            appStub.Setup(a => a.Forms.AddEx(It.IsAny<SAPbouiCOM.FormCreationParams>()))
                .Returns(() => new Mock<SAPbouiCOM.Form>().Object);
            appStub.Setup(a => a.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_FormCreationParams))
                .Returns(fcpStub.Object);

            return appStub;
        }

        void SetupFormDoesNotExist(Mock<SAPbouiCOM.Application> appStub)
        {
            appStub.Setup(a => a.Forms.Item(UID))
                .Throws(new Exception("form does not exist"));
        }

        void SetupFormExists(Mock<SAPbouiCOM.Application> appStub)
        {
            appStub.Setup(a => a.Forms.Item(UID))
                .Returns(() => new Mock<SAPbouiCOM.Form>().Object);
        }

        [Fact]
        void WithForm_WhenExistsNull_Throws()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);

            var service = new FormService(appStub.Object);
            Assert.Throws(typeof(ArgumentNullException),
                () => service.WithForm(UID, null, fcp => {}, form => {}));
        }

        [Fact]
        void WithForm_CreateNull_Throws()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);

            var service = new FormService(appStub.Object);
            Assert.Throws(typeof(ArgumentNullException),
                () => service.WithForm(UID, form => {}, null, form => {}));
        }

        [Fact]
        void WithForm_ConfigureNull_Throws()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);

            var service = new FormService(appStub.Object);
            Assert.Throws(typeof(ArgumentNullException),
                () => service.WithForm(UID, form => {}, fcp => {}, null));
        }

        [Fact]
        void WithForm_WhenFormDoesNotExist_FormCreationParamsCreated()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);
            var service = new FormService(appStub.Object);

            bool fcpCreated = false;
            service.WithForm(UID, form => {},
                fcp => {
                    if (fcp != null)
                        fcpCreated = true;
                },
                form => {});

            Assert.True(fcpCreated);
        }

        [Fact]
        void WithForm_WhenFormDoesNotExist_FormCreationParamsHasFormUID()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);
                
            var service = new FormService(appStub.Object);

            bool fcpHasUID = false;
            service.WithForm(UID, form => {},
                fcp => {
                    if (fcp != null && fcp.UniqueID != null && fcp.UniqueID.Equals(UID))
                        fcpHasUID = true;
                },
                form => {});

            Assert.True(fcpHasUID);
        }

        [Fact]
        void WithForm_WhenFormDoesNotExist_CreateCalled()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);
            var service = new FormService(appStub.Object);

            bool createCalled = false;
            service.WithForm(UID, form => {},
                fcp => { createCalled = true; },
                form => {});

            Assert.True(createCalled);
        }

        [Fact]
        void WithForm_WhenFormDoesNotExist_ConfigureCalled()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);
            var service = new FormService(appStub.Object);

            bool configureCalled = false;
            service.WithForm(UID, form => {},
                fcp => {},
                form => { configureCalled = true; });

            Assert.True(configureCalled);
        }

        [Fact]
        void WithForm_WhenFormDoesNotExist_ConfigureFormNotNull()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);
            var service = new FormService(appStub.Object);

            bool configureFormNotNull = false;
            service.WithForm(UID, form => {},
                fcp => {},
                form => { if (form != null) configureFormNotNull = true; });

            Assert.True(configureFormNotNull);
        }

        [Fact]
        void WithForm_WhenFormDoesNotExist_WhenExistsCalled()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);
            var service = new FormService(appStub.Object);

            bool whenExistsCalled = false;
            service.WithForm(UID, form => { whenExistsCalled = true; },
                fcp => {},
                form => {});

            Assert.True(whenExistsCalled);
        }

        [Fact]
        void WithForm_WhenFormDoesNotExist_WhenExistsFormNotNull()
        {
            var appStub = GetAppStub();
            SetupFormDoesNotExist(appStub);
            var service = new FormService(appStub.Object);

            bool whenFormNotNull = false;
            service.WithForm(UID, form => { whenFormNotNull = form != null; },
                fcp => {},
                form => {});

            Assert.True(whenFormNotNull);
        }

        [Fact]
        void WithForm_WhenFormExists_WhenExistsCalled()
        {
            var appStub = GetAppStub();
            SetupFormExists(appStub);
            var service = new FormService(appStub.Object);

            bool whenExistsCalled = false;
            service.WithForm(UID, form => { whenExistsCalled = true; },
                fcp => {},
                form => {});

            Assert.True(whenExistsCalled);
        }

        [Fact]
        void WithForm_WhenFormExists_WhenExistsFormNotNull()
        {
            var appStub = GetAppStub();
            SetupFormExists(appStub);
            var service = new FormService(appStub.Object);

            bool whenExistsFormNotNull = false;
            service.WithForm(UID, form => { whenExistsFormNotNull = form != null; },
                fcp => {},
                form => {});

            Assert.True(whenExistsFormNotNull);
        }

        [Fact]
        void WithForm_WhenFormExists_ConfigureNotCalled()
        {
            var appStub = GetAppStub();
            SetupFormExists(appStub);
            var service = new FormService(appStub.Object);

            bool configureCalled = false;
            service.WithForm(UID, form => {},
                fcp => {},
                form => { configureCalled = true; });

            Assert.False(configureCalled);
        }

        [Fact]
        void WithForm_WhenFormExists_CreateNotCalled()
        {
            var appStub = GetAppStub();
            SetupFormExists(appStub);
            var service = new FormService(appStub.Object);

            bool createCalled = false;
            service.WithForm(UID, form => {},
                fcp => { createCalled = true; },
                form => {});

            Assert.False(createCalled);
        }
    }
}
