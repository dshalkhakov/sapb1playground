﻿using System;
using Xunit;
using Moq;
using Xunit;

namespace B1Tests.Tests
{
    public class FluentFormTests
    {
        [Fact]
        public void AutoManage_Set()
        {
            var formStub = new Mock<SAPbouiCOM.Form>();
            formStub.SetupProperty(f => f.AutoManaged);

            B1Tests.FluentFormConfig.Configure(formStub.Object)
                .AutoManage(true);
            Assert.Equal(true, formStub.Object.AutoManaged);
        }

        [Fact]
        public void TitleSet()
        {
            var formStub = new Mock<SAPbouiCOM.Form>();
            formStub.SetupProperty(f => f.Title);

            B1Tests.FluentFormConfig.Configure(formStub.Object)
                .Title("Bar");
            Assert.Equal("Bar", formStub.Object.Title);
        }
    }
}
