﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Moq;

namespace B1Tests.Tests
{
    public class ConditionsBuilderTests
    {
        public SAPbouiCOM.Condition GetCondition()
        {
            var obj = new Mock<SAPbouiCOM.Condition>();
            obj.SetupAllProperties();
            return obj.Object;
        }

        public SAPbouiCOM.Conditions GetConditions()
        {
            var condsList = new List<SAPbouiCOM.Condition>();
            var obj = new Mock<SAPbouiCOM.Conditions>();
            obj.Setup(conds => conds.Add())
                .Returns(() => {
                    condsList.Add(GetCondition());
                    return condsList.Last();
                });
            obj.Setup(conds => conds.Item(It.IsAny<int>()))
                .Returns<int>(arg => condsList.ElementAtOrDefault(arg));

            return obj.Object;
        }

        [Fact]
        public void Build_WhenLiteral_ShouldRaiseException()
        {
            // should blow up
            ConditionsBuilder.Build(GetConditions(), aliases => true);
            Assert.False(true);
        }

        /// <summary>
        /// Literal lvar is not allowed.
        /// </summary>
        [Fact]
        public void Build_LvarLiteral_ShouldRaiseException()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => true == aliases["true"].Bool);
            Assert.False(true);
        }

        public void Build_NestedRval_NotAllowed()
        {
            // should not compile
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["true"].Bool == (aliases["A"].Bool || aliases["B"].Bool));
        }

        [Fact]
        public void Build_Nesting_Works()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => ((aliases["A"].Bool == true) || (aliases["B"].Bool == true)) || (aliases["C"].Bool == true));
        }

        [Fact]
        public void Build_ConstantsUnfolded()
        {
            // I wonder if in expression
            // 'aliases => alias[CONST] == CONST'
            // CONST would be a constant or a LOAD instruction
            // yes, it is a constant
            const string CONST = "CONST";
            ConditionsBuilder.Build(GetConditions(),
                aliases => aliases[CONST].Str == CONST);
            // it's a member expression with a variable
            //string NOT_SO_CONST = "CONST";
            //ConditionsBuilder.Build(GetConditions(),
            //    aliases => aliases[NOT_SO_CONST].Str == NOT_SO_CONST);

        }

        #region Comparison operators

        [Fact]
        public void Build_EqWithAlias_Works()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["a"] == aliases["b"]);
            Assert.False(true);
        }

        [Fact]
        public void Build_NotEqWithAlias_Works()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["a"] != aliases["b"]);
            Assert.False(true);
        }

        [Fact]
        public void Build_EqWithVar_Works()
        {
            var conds = GetConditions();
            ConditionsBuilder.Build(conds, aliases => aliases["a"].Str == "a");
            Mock.Get<SAPbouiCOM.Conditions>(conds)
                .Verify(c => c.Add(), Times.Exactly(1));

            var obj = Mock.Get<SAPbouiCOM.Conditions>(conds).Object;
            var item = obj.Item(0);
            Assert.Equal(1, item.BracketOpenNum);
            Assert.Equal(1, item.BracketCloseNum);
            Assert.Equal("a", item.CondVal);
            Assert.Equal(false, item.CompareFields);
            Assert.Equal(SAPbouiCOM.BoConditionOperation.co_EQUAL, item.Operation);
            Assert.Equal(SAPbouiCOM.BoConditionRelationship.cr_NONE, item.Relationship);
            Assert.Equal("a", item.Alias);
        }

        [Fact]
        public void Build_NotEqWithVar_Works()
        {
            var conds = GetConditions();
            ConditionsBuilder.Build(conds, aliases => aliases["a"].Str != "a");
            Mock.Get<SAPbouiCOM.Conditions>(conds)
                .Verify(c => c.Add(), Times.Exactly(1));
        }

        [Fact]
        public void Build_Gte_Works()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["a"].Int >= 1);
        }

        [Fact]
        public void Build_Lte_Works()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["a"].Int <= 1);
        }

        [Fact]
        public void Build_MoreThan_Works()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["a"].Int > 1);
        }

        [Fact]
        public void Build_LessThan_Works()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["a"].Int < 1);
        }

        #endregion

        #region String comparison

        [Fact]
        public void Build_LikeWorks()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["A"].Like("%B%"));
        }

        [Fact]
        public void Build_NotLikeWorks()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["A"].NotLike("%B%"));
        }

        [Fact]
        public void Build_BeginsWithWorks()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["ABCDEF"].BeginsWith("AB"));
        }

        [Fact]
        public void Build_EndsWithWorks()
        {
            ConditionsBuilder.Build(GetConditions(), aliases => aliases["ABCDEF"].EndsWith("EF"));
        }

        #endregion

        #region Complex expressions

        [Fact]
        public void Build_ComplexExpression_Works()
        {
            var conds = GetConditions();
            ConditionsBuilder.Build(conds, aliases =>
                (aliases["foo"].Str == "A" && aliases["bar"].Str == "B"));
            // output:

            Mock.Get<SAPbouiCOM.Conditions>(conds)
                .Verify(c => c.Add(), Times.Exactly(2));

            var obj = Mock.Get<SAPbouiCOM.Conditions>(conds).Object;
            //  [0] condition with relationship = 'AND', BracketOpenNum = 1, BracketCloseNum = 2
            {
                var item = obj.Item(0);
                Assert.Equal(2, item.BracketOpenNum);
                Assert.Equal(1, item.BracketCloseNum);
                Assert.Equal("A", item.CondVal);
                Assert.Equal(false, item.CompareFields);
                Assert.Equal(SAPbouiCOM.BoConditionOperation.co_EQUAL, item.Operation);
                Assert.Equal(SAPbouiCOM.BoConditionRelationship.cr_AND, item.Relationship);
                Assert.Equal("foo", item.Alias);
            }
            //  [1] condition with relationship = 0, BracketOpenNum = 1, BracketCloseNum = 2
            {
                var item = obj.Item(1);
                Assert.Equal(1, item.BracketOpenNum);
                Assert.Equal(2, item.BracketCloseNum);
                Assert.Equal("B", item.CondVal);
                Assert.Equal(false, item.CompareFields);
                Assert.Equal(SAPbouiCOM.BoConditionOperation.co_EQUAL, item.Operation);
                Assert.Equal(SAPbouiCOM.BoConditionRelationship.cr_NONE, item.Relationship);
                Assert.Equal("bar", item.Alias);
            }
        }

        #endregion

        #region Nested expressions

        [Fact]
        public void Build_NestedExpression_Works()
        {
            // FIXME not sure if nested expression are supported
            // SAP might as well require you to input conditions in disjunctive/conjunctive normal form
            ConditionsBuilder.Build(GetConditions(), aliases =>
                (aliases["foo"].Str == "A" && aliases["bar"].Str == "B")
                || (aliases["foo"].Str == "B" && aliases["bar"].Str == "A"));
            // output:
            // conditions
            //  [0] condition with relationship = 'OR'
            //  [1] condition
        }

        [Fact]
        public void Build_NestedExpression_ParensIndexesAreCorrect()
        {

        }

        #endregion
    }
}
