﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using B1Tests;

namespace B1Tests.Tests
{
    public class FluentUserTableTests
    {
        [Fact]
        public void Test()
        {
            var tableITS_DI_DataCommission = "ITS_IOC_CONT";
            var tableMDSpy = new Moq.Mock<SAPbobsCOM.UserTablesMD>();
            var companySpy = new Moq.Mock<SAPbobsCOM.Company>(Moq.MockBehavior.Strict);

            companySpy.Setup(c => c.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserFields))
                .Returns(() => {
                    var obj = new Moq.Mock<SAPbobsCOM.UserFieldsMD>();
                    return obj.Object;
                });
            companySpy.Setup(c => c.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables))
                .Returns(tableMDSpy.Object);

            var builder = new FluentUserTableMDBuilder(companySpy.Object);
            var table = builder.WithNewDocumentTable(tableITS_DI_DataCommission)
                .TableDescription("Инвент ДС - Данные и коммисия")
                .TableType(SAPbobsCOM.BoUTBTableType.bott_Document);
            table.Add();

            table.WithDateField("DatePD")
                .Description("Дата подсчета")
                .Mandatory()
                .EditSize(64)
                .Add();

            table.WithTimeField("Time")
                .Description("Время")
                .Add();

            table.WithField("Status")
                .Description("Статус")
                .EditSize(15)
                .Add();

            table.WithField("Invent")
                .Description("Инвентаризатор")
                .EditSize(12)
                .Add();

            table.WithField("CashAcc")
                .Description("Счет кассы")
                .EditSize(15)
                .Add();

            table.WithField("NmbrOrd")
                .Description("Номер приказа")
                .EditSize(15)
                .Add();

            table.WithDateField("DateOrd")
                .Description("Дата приказа")
                .Add();

            table.WithField("RootComm")
                .Description("Председатель комиссии")
                .EditSize(12)
                .Add();
            table.WithField("m1_Comm")
                .Description("Члены комиссии 1")
                .EditSize(12)
                .Add();
            table.WithField("m2_Comm")
                .Description("Члены комиссии 2")
                .EditSize(12)
                .Add();
            table.WithField("m3_Comm")
                .Description("Члены комиссии 3")
                .EditSize(12)
                .Add();
        }
    }
}
