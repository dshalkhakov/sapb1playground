﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace B1Tests.Tests
{
#if ZERO
    public class Model
    {

    }

    public class View
    {

    }

    // we have very generic forms
    // Application.Forms.Index("uid")
    // one can do everything with it

    // what is view?
    // active form with its' elements
    // a class that takes data from the model and 'renders' it by setting form properties
    // in ideal case, should be done purely w/ data binding
    // view :: Model -> IO ()
    // rendering is done with IActiveFormService, too. hmm...

    // what is model?
    // hmm. active form with its' elements :)
    // currently, IActiveFormService serves both as a view and as a model. Some actions query
    // the server with DI Recordset interface. And then manually change the
    // view with IActiveFormService interface. There's also UDO interface
    // serving the same purpose.
    // Data sources serve as models, apparently [1]. Matrices serve as views.

    // SAPbouiCOM controls can be both models and views.

    // [1] https://yalantis.com/blog/lightweight-ios-view-controllers-separate-data-sources-guided-mvc/
    
    // what is controller?
    // currently, we only have one. VidsSamp.Vids.
    // action routing is implemented in SBO_Application_ItemEvent() private
    // method.

    public class Controller
    {
        void Action1() { }
        void Action2() { }
    }

    // from 

    public interface IReplaceView
    {
        bool EnableCreate { get; set; }
        bool EnableDelete { get; set; }

        List<ReplaceSymbol> Replaces {
            set;
        }

        ReplaceSymbol FocusedReplace { get; set; }

        List<ReplaceSymbol> SelectedReplaces { get; set; }

        event EventHandler Closing;
        event EventHandler Creating;
        event EventHandler Deleting;
        event EventHandler FocusedChanged;

        void Open();
        void Close();
        void RefreshView();
    }

    public class ReplaceController : IReplaceController
    {
        private IReplaceView _view = null;
        private IReplaceStorage _replaceStorage = null;

        public ReplaceController(IReplaceView view, IReplaceStorage replaceStorage)
        {
            Contract.Requires(view != null, "View can’t be null");
            Contract.Requires(replaceStorage != null, "ReplaceStorage can’t be null”);

            _view = view;
            _replaceStorage = replaceStorage;
        }
    }

    public class MVCTest1
    {
        [Xunit.Fact]
        public void LoadEmptyReplaces()
        {
            var emptyReplaces = new List<ReplaceSymbol>();
            var storage = new StubIReplaceStorage()
            {
                ReplaceSymbolsGet =
                () => {
                    return emptyReplaces;
                },
            };

            ReplaceSymbol focusedReplace = null;
            var loadedReplaces = new List<ReplaceSymbol>();
            var selectedReplaces = new List<ReplaceSymbol>();
            var enableCreate = false;
            var enableDelete = false;

            var view = new StubIReplaceView()
            {
                FocusedReplaceGet =
                () => {
                    return focusedReplace;
                },
                FocusedReplaceSetReplaceSymbol =
                x =>
                {
                    focusedReplace = x;
                    if (x != null)
                    {
                        selectedReplaces.Add(x);
                    }
                },
                ReplacesSetListOfReplaceSymbol =
                x =>
                {
                    loadedReplaces = x;
                },
                EnableCreateSetBoolean =
                x =>
                {
                    enableCreate = x;
                },
                EnableDeleteSetBoolean =
                x =>
                {
                    enableDelete = x;
                },
                SelectedReplacesGet =
                () =>
                {
                    return selectedReplaces;
                },
            };

            var controller = new ReplaceController(view, storage);
            controller.Open();
            view.FocusedChangedEvent(null, null);

            Assert.IsNotNull(loadedReplaces, "После создания контроллераи и его открытия список замен не должен быть null");
            Assert.IsTrue(loadedReplaces.Count == 0, "После создания контроллера и его открытия список замен не должен быть пустым, так как в него загружен пустой список");
            Assert.IsNull(focusedReplace, "После создания контроллера и его открытия активной замены быть не должно");
            Assert.IsNotNull(selectedReplaces, "После создания контроллера список замен не может быть null");
            Assert.IsTrue(selectedReplaces.Count == 0, "После создания контроллера и его открытия в список выбраных замен должен быть пустой");
            Assert.IsTrue(enableCreate, "После создания контроллера должна быть доступна возможность создавать замены");
            Assert.IsFalse(enableDelete, "После создания контроллера не должно быть доступно удаление, так как список замен пустой");
        }
    }
#endif
}
